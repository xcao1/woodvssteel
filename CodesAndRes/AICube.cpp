#include "AICube.h"


AICube::AICube():danger(false)
{
	//set color
	itsfacetcolor = color4(1,0,0,1);
	itswirecolor = color4(1,1,1,1);
	//size of the cube
	itssize = 1.0;
	int index=0;
	//vertex
	itspoints[index]=point4(0,0,0,1);index++;
	itspoints[index]=point4(itssize,0,0,1);index++;
	itspoints[index]=point4(0,0,itssize,1);index++;
	itspoints[index]=point4(itssize,0,itssize,1);index++;
	itspoints[index]=point4(0,itssize,0,1);index++;
	itspoints[index]=point4(itssize,itssize,0,1);index++;
	itspoints[index]=point4(0,itssize,itssize,1);index++;
	itspoints[index]=point4(itssize,itssize,itssize,1);index++;

	index=0;
	//facet vertices buffer
	//facet 1 bottom
	itsfacetvertices[index]=itspoints[0];tex_coords[index]=tex2(0,0);index++;
	itsfacetvertices[index]=itspoints[1];tex_coords[index]=tex2(1,0);index++;
	itsfacetvertices[index]=itspoints[3];tex_coords[index]=tex2(1,1);index++;
	itsfacetvertices[index]=itspoints[2];tex_coords[index]=tex2(0,1);index++;
	//facet 2 top
	itsfacetvertices[index]=itspoints[4];tex_coords[index]=tex2(0,0);index++;
	itsfacetvertices[index]=itspoints[5];tex_coords[index]=tex2(1,0);index++;
	itsfacetvertices[index]=itspoints[7];tex_coords[index]=tex2(1,1);index++;
	itsfacetvertices[index]=itspoints[6];tex_coords[index]=tex2(0,1);index++;
	//facet 3
	itsfacetvertices[index]=itspoints[0];tex_coords[index]=tex2(0,0);index++;
	itsfacetvertices[index]=itspoints[1];tex_coords[index]=tex2(1,0);index++;
	itsfacetvertices[index]=itspoints[5];tex_coords[index]=tex2(1,1);index++;
	itsfacetvertices[index]=itspoints[4];tex_coords[index]=tex2(0,1);index++;
	//facet 4
	itsfacetvertices[index]=itspoints[0];tex_coords[index]=tex2(0,0);index++;
	itsfacetvertices[index]=itspoints[4];tex_coords[index]=tex2(1,0);index++;
	itsfacetvertices[index]=itspoints[6];tex_coords[index]=tex2(1,1);index++;
	itsfacetvertices[index]=itspoints[2];tex_coords[index]=tex2(0,1);index++;
	//facet 5
	itsfacetvertices[index]=itspoints[1];tex_coords[index]=tex2(0,0);index++;
	itsfacetvertices[index]=itspoints[5];tex_coords[index]=tex2(1,0);index++;
	itsfacetvertices[index]=itspoints[7];tex_coords[index]=tex2(1,1);index++;
	itsfacetvertices[index]=itspoints[3];tex_coords[index]=tex2(0,1);index++;
	//facet 6
	itsfacetvertices[index]=itspoints[2];tex_coords[index]=tex2(0,0);index++;
	itsfacetvertices[index]=itspoints[6];tex_coords[index]=tex2(1,0);index++;
	itsfacetvertices[index]=itspoints[7];tex_coords[index]=tex2(1,1);index++;
	itsfacetvertices[index]=itspoints[3];tex_coords[index]=tex2(0,1);index++;

	//wire vertices buffer
	//itswirevertices
	index=0;
	//line 0
	itswirevertices[index]=itspoints[0];index++;
	itswirevertices[index]=itspoints[1];index++;
	//line 1
	itswirevertices[index]=itspoints[0];index++;
	itswirevertices[index]=itspoints[2];index++;
	//line 3
	itswirevertices[index]=itspoints[2];index++;
	itswirevertices[index]=itspoints[3];index++;
	//line 4
	itswirevertices[index]=itspoints[1];index++;
	itswirevertices[index]=itspoints[3];index++;


	//line 5
	itswirevertices[index]=itspoints[4];index++;
	itswirevertices[index]=itspoints[5];index++;
	//line 6
	itswirevertices[index]=itspoints[4];index++;
	itswirevertices[index]=itspoints[6];index++;
	//line 7
	itswirevertices[index]=itspoints[5];index++;
	itswirevertices[index]=itspoints[7];index++;
	//line 8
	itswirevertices[index]=itspoints[6];index++;
	itswirevertices[index]=itspoints[7];index++;


	//line 9
	itswirevertices[index]=itspoints[0];index++;
	itswirevertices[index]=itspoints[4];index++;
	//line 10
	itswirevertices[index]=itspoints[2];index++;
	itswirevertices[index]=itspoints[6];index++;
	//line 11
	itswirevertices[index]=itspoints[3];index++;
	itswirevertices[index]=itspoints[7];index++;
	//line 12
	itswirevertices[index]=itspoints[1];index++;
	itswirevertices[index]=itspoints[5];index++;
}

AICube::~AICube()
{
}

void AICube::OnInit()
{
	//read the texture form file
	image = glmReadPPM("steel_texture.ppm",width, height);
	glGenTextures( 1, &texture );

	//initialize buffers
	glGenBuffers( 2, buffers );
	
	//initialize VAOs
	glGenVertexArrays( 2, vaos );
	
	//initialize shaders
	facetprogram = InitShader( "cubefacet.v", "cubefacet.f" );
	wireprogram = InitShader( "cubeframe.v", "cubeframe.f" );

	GLuint vPosition;
	//vao0 cubefacet
	glUseProgram( facetprogram );
	glBindVertexArray( vaos[0] );

	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texture );
	
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB,width,height, 0, GL_RGB, GL_UNSIGNED_BYTE, image );

	glBindBuffer( GL_ARRAY_BUFFER, buffers[0] );
	glBufferData( GL_ARRAY_BUFFER, sizeof(itsfacetvertices)+sizeof(tex_coords), NULL, GL_STATIC_DRAW );
	
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(itsfacetvertices), itsfacetvertices );
	glBufferSubData( GL_ARRAY_BUFFER, sizeof(itsfacetvertices), sizeof(tex_coords), tex_coords );
	vPosition = glGetAttribLocation( facetprogram, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	vTexCoord = glGetAttribLocation( facetprogram, "vTexCoord" );
    glEnableVertexAttribArray( vTexCoord );
    glVertexAttribPointer( vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(itsfacetvertices)) );

    glUniform1i( glGetUniformLocation(facetprogram, "texture"), 0 );
	
	//vao1 cubeframe
	glUseProgram( wireprogram );
	glBindVertexArray( vaos[1] );
	glBindBuffer( GL_ARRAY_BUFFER, buffers[1] );
	glBufferData( GL_ARRAY_BUFFER, sizeof(itswirevertices), NULL, GL_STATIC_DRAW );
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(itswirevertices), itswirevertices );
	vPosition = glGetAttribLocation( wireprogram, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	

}
void AICube::OnDisplay(mat4 _input_matrix_)
{
	mat4 projection = mat4(1) * Perspective(90,1/1,1.0,40.0);

	glUseProgram( facetprogram );
	glUniformMatrix4fv(glGetUniformLocation( facetprogram, "ModelView" ), 1, GL_TRUE, _input_matrix_ );
	glUniformMatrix4fv(glGetUniformLocation( facetprogram, "Projection" ), 1, GL_TRUE, projection );
	
	//if in danger show red color to warn
	switch(danger)
	{
	case true:
		{
			glUniform4fv(glGetUniformLocation(facetprogram, "color"), 1, itsfacetcolor);
			break;
		}
	case false:
		{
			glUniform4fv(glGetUniformLocation(facetprogram, "color"), 1, color4(1,1,1,1));
			break;
		}
	}
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0, 1.0);
	glBindVertexArray( vaos[0] );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texture );
	glDrawArrays( GL_QUADS, 0, 4*6 );
	glDisable(GL_POLYGON_OFFSET_FILL);


	glUseProgram( wireprogram );
	glUniformMatrix4fv(glGetUniformLocation( wireprogram, "ModelView" ), 1, GL_TRUE, _input_matrix_ );
	glUniformMatrix4fv(glGetUniformLocation( wireprogram, "Projection" ), 1, GL_TRUE, projection );
	glUniform4fv(glGetUniformLocation(wireprogram, "color"), 1, itswirecolor);
	glBindVertexArray( vaos[1] );
	glDrawArrays( GL_LINES, 0, 12*2 );

}

void AICube::changestatus(bool _c)
{
	danger = _c;
}