#include "AITurnState.h"

AITurnState::AITurnState()
{
	itsName = "AITurn";
	GOTONEXT = 0;
}
AITurnState::~AITurnState()
{
}
void AITurnState::Init(SceneManager *_input_SceneManager_)
{
	for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
			iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
	{
		if((*iterAI).Dead == false)
		{
			//recover health
			(*iterAI).HP += 1;
			//refresh active
			(*iterAI).Active = 0;
		}
		
	}
	GOTONEXT = 0;
}
void AITurnState::Clear()
{
}
void AITurnState::OnDisplay(SceneManager *_input_SceneManager_)
{
	
}
void AITurnState::OnIdle(SceneManager *_input_SceneManager_)
{
	nextStateName = "AIUnitQueue";
	GOTONEXT = 1;
	glutPostRedisplay();
}