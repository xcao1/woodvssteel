#ifndef _AI_TURN_STATE_H_
#define _AI_TURN_STATE_H_
//
#include "State.h"
//
//Recovery every AI Units' HP
//Refresh Active to 0
class AITurnState:public State
{
protected:
public:
	AITurnState();
	~AITurnState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
};
//
#endif 