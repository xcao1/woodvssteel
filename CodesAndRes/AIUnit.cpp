#include "AIUnit.h"

AIUnit::AIUnit():CubeActor(),HP(10),ATK(2),Range(1),Speed(4),itsCube(),Active(0),Dead(false)
{
}
AIUnit::AIUnit(int _input_number_):CubeActor(_input_number_),HP(10),ATK(2),Range(1),Speed(4),itsCube(),Active(0),Dead(false)
{
}
AIUnit::AIUnit(int _input_number_,point4 _input_position_):CubeActor(_input_number_,_input_position_),HP(10),ATK(2),Range(1),Speed(4),Dead(false),itsCube(),Active(0)
{
}
AIUnit::~AIUnit()
{
}
void AIUnit::ReSet()
{
	//reset
	HP = 18;
	ATK = 2;
	Range = 1;
	Speed = 4;
	Dead = false;
	Active = 0;
}

void AIUnit::OnInit()
{
	itsCube.OnInit();
}
void AIUnit::OnDisplay(mat4 _m)
{
	//if in danger ,warn
	if(HP < 6)
	{
		itsCube.changestatus(true);
	}
	else
	{
		itsCube.changestatus(false);
	}
	itsCube.OnDisplay(_m);
}