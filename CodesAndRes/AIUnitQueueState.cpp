#include "AIUnitQueueState.h"

AIUnitQueueState::AIUnitQueueState()
{
	GOTONEXT = 0;
	itsName = "AIUnitQueue";
}
AIUnitQueueState::~AIUnitQueueState()
{
}
void AIUnitQueueState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
	int i = 0;
	//check if all units has actived,
	//if so,go to player's turn
	int actived = 0;
	for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
			iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
	{
		i++;
		if((*iterAI).Active == -1 || (*iterAI).Dead == true)

		{
			actived++;
		}
	}
	if(actived >= i)
	{
		nextStateName = "PlayerTurn";
		GOTONEXT = 1;
	}
}
void AIUnitQueueState::Clear()
{
}
void AIUnitQueueState::OnDisplay(SceneManager *_input_SceneManager_)
{
}
void AIUnitQueueState::OnIdle(SceneManager *_input_SceneManager_)
{
	for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
			iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
	{
		if((*iterAI).Dead == false)
		{
			if((*iterAI).Active == 0)
			{
				//pause for a short time to show every units' behaviour 
				Sleep(700);
				(*iterAI).Active = 1;
				nextStateName = "SearchAndAttack";
				GOTONEXT = 1;
				glutPostRedisplay();
				break;
			}
		}
	}
}
void AIUnitQueueState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
}
void AIUnitQueueState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}