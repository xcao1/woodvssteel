#ifndef _AI_QUEUE_STATE_H_
#define _AI_QUEUE_STATE_H_
//
#include "State.h"
//
//In this state ,The AI units's queue will
//turn its unit into active one by one 
//and go to next state "SearchAndAttack"
//If all units has actived,and finished their turn
//the nextstate will be "PlayerTurn"
class AIUnitQueueState:public State
{
protected:
public:
	AIUnitQueueState();
	~AIUnitQueueState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif