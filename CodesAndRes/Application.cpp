#include "Application.h"

Application::Application():itsSceneManager(),itsStateMachine()
{
}
Application::~Application()
{
}
//Use this function to set the scene
void Application::init(void)
{
	//Create the mesh unit,and set their location
	itsSceneManager.CreateMeshUnit(point4(0,0,0,1));
	itsSceneManager.CreateMeshUnit(point4(0,-15,0,1));
	itsSceneManager.CreateMeshUnit(point4(-15,0,0,1));
	itsSceneManager.CreateMeshUnit(point4(-15,-15,0,1));

	//Create the player's unit,and set their location
	itsSceneManager.CreatePlayerUnit(point4(-5,0,0,1));
	itsSceneManager.CreatePlayerUnit(point4(-5,1,0,1));
	itsSceneManager.CreatePlayerUnit(point4(-5,2,0,1));
	itsSceneManager.CreatePlayerUnit(point4(-5,3,0,1));
	
	//Create the AI's unit, and set their location
	itsSceneManager.CreateAIUnit(point4(4,0,0,1));
	itsSceneManager.CreateAIUnit(point4(3,1,0,1));
	itsSceneManager.CreateAIUnit(point4(3,2,0,1));
	itsSceneManager.CreateAIUnit(point4(2,2,0,1));
	itsSceneManager.CreateAIUnit(point4(3,3,0,1));
	itsSceneManager.CreateAIUnit(point4(4,4,0,1));
	
	
	//Create the background and set location
	itsSceneManager.AjustBackGround(point4(-25,-25,-1,1));
	
	//initial all
	itsSceneManager.OnInit();
	//the first time ,to begin from the start state
	itsStateMachine.Begin(&itsSceneManager);
	
	//defualt
	glEnable(GL_TEXTURE_2D);
	glEnable( GL_DEPTH_TEST );
	glDepthFunc(GL_LEQUAL);
	glShadeModel (GL_SMOOTH);
	glShadeModel(GL_FLAT);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glClearColor( 0.0, 0.0, 0.0, 1.0 );
	glClearDepth( 1.0 ); 
}
void Application::display(void)
{
	//defualt
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	//display the scene
	itsSceneManager.DisplayItem();
	//current state's display,display menu
	itsStateMachine.OnDisplay(&itsSceneManager);
	//defualt
	glutSwapBuffers();
}
void Application::keyboard(unsigned char key)
{
	//press to exit
	if(key == 033)
	{
		exit( EXIT_SUCCESS );
	}
	//current state's normal key interact
	itsStateMachine.OnNormalKey(&itsSceneManager,key);
}
void Application::specialkey(int key)
{
	//current state 's special key interact
	itsStateMachine.OnSpecialKey(&itsSceneManager,key);
}
void Application::idle(void)
{
	//rotate the cursor all the time
	itsSceneManager.CursorRotate();
	//current state's idle 
	itsStateMachine.OnIdle(&itsSceneManager);
	glutPostRedisplay();
}