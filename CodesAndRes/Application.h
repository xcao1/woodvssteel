#ifndef _APPLICATION_H_
#define _APPLICATION_H_
//
#include "Base.h"
#include "SceneManager.h"
#include "StateMachine.h"
//
//Store SceneManger and StateMachine
//The Scene can be easily setted in this class
//in its init function
//since it hold the SceneManager as it member
//for example:
//you can create Units Or GroundMeshes and locate
//them to the position you want
//This class is also the topest class in this engine
//it will communicate with the main function directly
class Application
{
protected:
	SceneManager itsSceneManager;
	StateMachine itsStateMachine;
public:
	Application();
	~Application();

	void init(void);
	void display(void);
	void keyboard(unsigned char key);
	void specialkey(int key);
	void idle(void);
};
//
#endif