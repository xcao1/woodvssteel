#include "AttackState.h"

AttackState::AttackState()
{
	GOTONEXT = 0;
	itsName = "Attack";
}
AttackState::~AttackState()
{
}
void AttackState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Active == 1)
		{
			//find out the active unit
			itsiterplayer = iterplayer;
		}
	}
	//move cursor to the unit position
	_input_SceneManager_->itsCursorUnit.itsPosition.x = (*itsiterplayer).itsPosition.x;
	_input_SceneManager_->itsCursorUnit.itsPosition.y = (*itsiterplayer).itsPosition.y;
	_input_SceneManager_->itsCursorUnit.ChangeStatus(true);

}
void AttackState::Clear()
{
}
void AttackState::OnDisplay(SceneManager *_input_SceneManager_)
{

}
void AttackState::OnIdle(SceneManager *_input_SceneManager_)
{
}
void AttackState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch (key)
	{
	case 'x':
		{
			//back,cancel order
			if((*itsiterplayer).moved == true)
			{
				nextStateName = "WaitOrAttack";
			}
			else
			{
				nextStateName = "PlayerUnitBehaviourSelect";
			}
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	case 'z':
		{
			for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
			iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
			{
				if((*iterAI).Dead == false)
				{
					if((*iterAI).itsPosition.x == _input_SceneManager_->itsCursorUnit.itsPosition.x &&
						(*iterAI).itsPosition.y == _input_SceneManager_->itsCursorUnit.itsPosition.y)
					{
						itsiterAI = iterAI;
						//attacking on it,it lose its health
						(*itsiterAI).HP = (*itsiterAI).HP - (*itsiterplayer).ATK;
						if((*itsiterAI).HP <= 0)
						{
							//if health below 0,then it will die
							//player will gain points
							(*itsiterAI).Dead = true;
							_input_SceneManager_->itsScore.score +=5;

						}
						//if all enemy destoried,if so ,you win
						bool win = true;
						for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
						iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
						{
							if((*iterAI).Dead == false)
							{
								win = false;
							}
						}
						if(win)
						{
							//gain additional 20 points
							_input_SceneManager_->itsScore.score +=20;
							_input_SceneManager_->itsScore.update();

							cout<<_input_SceneManager_->itsScore.scorestr<<endl;
							nextStateName = "Win";
							GOTONEXT = 1;
						}
						else
						{
							nextStateName = "Wait";
							GOTONEXT = 1;
						}
					}
				}
			}			
			glutPostRedisplay();
			break;
		}
	}
}
bool AttackState::Moveable(SceneManager *_input_SceneManager_,int x,int y)
{
	double TempDistance = 
		sqrt(pow(((_input_SceneManager_->itsCursorUnit.itsPosition.x + x) - (*itsiterplayer).itsPosition.x),2))+
		sqrt(pow(((_input_SceneManager_->itsCursorUnit.itsPosition.y + y) - (*itsiterplayer).itsPosition.y),2));
	if(TempDistance > (*itsiterplayer).Range)
	{
		return 0;
	}
	else
	{
		//able to move 
		return 1;
	}
	
}

void AttackState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
	switch( key )
	{
		//the object
		case GLUT_KEY_UP : 
			{
				// MOVE FORWARD
				if(Moveable(_input_SceneManager_,0,1))
				{
					_input_SceneManager_->itsCursorUnit.Move(0,1);
					glutPostRedisplay();
				}
				break;
			}
		case GLUT_KEY_DOWN :
			{
				// MOVE WARD
				if(Moveable(_input_SceneManager_,0,-1))
				{
					_input_SceneManager_->itsCursorUnit.Move(0,-1);
					glutPostRedisplay();
				}
				break;
			}
		case GLUT_KEY_LEFT:
			{
				// MOVE LEFT
				if(Moveable(_input_SceneManager_,-1,0))
				{
					_input_SceneManager_->itsCursorUnit.Move(-1,0);
					glutPostRedisplay();
				}
				break;
			}
		case GLUT_KEY_RIGHT:
			{
				// MOVE RIGHT
				if(Moveable(_input_SceneManager_,1,0))
				{
					_input_SceneManager_->itsCursorUnit.Move(1,0);
					glutPostRedisplay();
				}
				break;
			}
	}
}