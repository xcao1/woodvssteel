#ifndef _ATTACK_STATE_H_
#define _ATTACK_STATE_H_
//
#include "State.h"
//
//This State manager the operation
//while Player is Attacking
//And it will check if the player has won or not
//While Attacking,each attacking will cause enemy lose its health
//which is HP ,if HP below 0,the enemy will die,and the player will
//get some reward on his score,
//if he win the game ,he will get an additional 20 points to his score.
class AttackState:public State
{
protected:
public:
	vector<PlayerUnit>::iterator itsiterplayer;
	vector<AIUnit>::iterator itsiterAI;
	AttackState();
	~AttackState();
	bool Moveable(SceneManager *_input_SceneManager_,int x,int y);
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif