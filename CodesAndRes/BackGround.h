#ifndef _BACK_GROUND_H_
#define _BACK_GROUND_H_
//
#include "Base.h"
//
//used to draw BackGround
class BackGround
{
protected:
	point4 itspoints[4];

	
	tex2 texturecoordinates[4];
	GLuint vTexCoord;
	GLsizei width;
	GLsizei height;
	GLuint texture;
	GLubyte *image;

	//vao and vbo
	GLuint vaos[2];
	GLuint buffers[2];
	//programs
	GLuint facetprogram;
public:
	BackGround();
	~BackGround();
	virtual void OnInit();
	virtual void OnDisplay(mat4 _input_matrix_);
};
//
#endif