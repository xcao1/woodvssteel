#include "BackGroundUnit.h"

BackGroundUnit::BackGroundUnit()
{
	itsPosition = point4(0,0,0,1);
}
BackGroundUnit::~BackGroundUnit()
{
}

void BackGroundUnit::OnInit()
{
	_background.OnInit();
}

void BackGroundUnit::OnDisplay(mat4 _m)
{
	_background.OnDisplay(_m);
}