#ifndef _BACK_GROUND_UNIT_H_
#define _BACK_GROUND_UNIT_H_
//
#include "BackGround.h"
//
class BackGroundUnit
{
protected:
	
public:

	point4 itsPosition;
	BackGround _background;


	BackGroundUnit();
	~BackGroundUnit();

	void OnInit();
	void OnDisplay(mat4 _m);
};
//
#endif