#ifndef _BASE_H_
#define _BASE_H_
//
//Store some very basic head files
//And define some very basic types
//That will be used frequently in this engine

#include <windows.h>
#include <time.h>
#include<math.h>
#include<iostream>
#include <vector>
#include<string>
#include <stdlib.h>
#include "Angel.h"
#include "glm.h"
//
typedef vec4 point4;
typedef vec3 point3;
typedef vec2 point2;
typedef vec4 normal4;
typedef vec4 color4;
typedef vec2 tex2;
//
using namespace std;
using namespace Angel;






#include "Light.h"
extern Light mLight;
//
#endif