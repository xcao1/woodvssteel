#include "Camera.h"

Camera::Camera():default_position(point4(0,0,10,1)),move_x(0),move_y(0),roll_z(0),elevate_z(0),reset(false)
{
	//looking direction always along z axis
	//will not change
	camera_matrix = mat4(1) * Translate(-default_position.x,-default_position.y,-default_position.z);
}
Camera::~Camera()
{
}

void Camera::range(float &_move_x,float &_move_y,float &_roll_x,float &_roll_z,float &_elevate_z)
{
	//do not move to far away from the center of the scene
	if(_move_x > 16) _move_x = 16;
	if(_move_x < -16) _move_x = -16;
	if(_move_y > 16) _move_y = 16;
	if(_move_y < -16) _move_y = -16;


	//rotate form 90 - 0
	if(_roll_x >0) _roll_x = 0;
	if(_roll_x < -90) _roll_x = -90;
	//roll it whatever
	if(_roll_z > 360) _roll_z = 0;
	if(_roll_z < 0) _roll_z = 360; 

	//don't be too high or too low
	if(_elevate_z > 8) _elevate_z = 8;
	if(_elevate_z < -8) _elevate_z = -8;
}
//generate the matrix
mat4 Camera::getmatrix()
{
	//limit the control
	range(move_x,move_y,roll_x,roll_z,elevate_z);

	if(reset == false)
	{
		camera_matrix = mat4(1) * Translate(-(default_position.x+move_x),-(default_position.y+move_y),-(default_position.z + elevate_z)) * RotateX(roll_x) * RotateZ(roll_z);
	}
	else if(reset == true)
	{
		//reset to origin
		camera_matrix = mat4(1) * Translate(-default_position.x,-default_position.y,-default_position.z);
	}
	return camera_matrix;
}
void Camera::control(float _x,float _y,float _rx,float _rz,float _el,bool _reset)
{
	move_x = move_x + _x;
	move_y = move_y + _y;

	roll_x = roll_x - _rx;
	roll_z = roll_z + _rz;
	elevate_z = elevate_z - _el;
	reset = _reset;

	if(reset == true)
	{
		//reset to origin
		roll_x = 0;
		roll_z = 0;
		elevate_z = 0;
	}
	range(move_x,move_y,roll_x,roll_z,elevate_z);
	
}
void Camera::moveto(point4 _position)
{
	move_x = _position.x;
	move_y = _position.y;

	range(move_x,move_y,roll_x,roll_z,elevate_z);
}