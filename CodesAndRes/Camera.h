#ifndef _CAMERA_H_
#define _CAMERA_H_
//
#include "Base.h"
//
//Used to control the camera
//
class Camera
{
private:
	
protected:
	//ATTRIBUTES:
	//height 10 above the meshes
	point4 default_position;
	//looking direction always along z axis
	//will not change
	
	//controller
	//move and rotate
	float move_x;
	float move_y;
	float roll_x;
	float roll_z;
	float elevate_z;
	
	//reset
	bool reset;

	//The Matrix that will be applied to each objects
	//so that they will be affected,and the viewer will
	//find he is controlling the camera
	mat4 camera_matrix;
public:
	
	//FUNCTIONS:
	//constructors:
	Camera();
	~Camera();
	//member functions:
	//limit the control of the camera
	void range(float &_move_x,float &_move_y,float &_roll_x,float &_roll_z,float &_elevate_z);
	mat4 getmatrix();
	void control(float _x,float _y,float _rx,float _rz,float _el,bool _reset);
	void moveto(point4 _position);
};
//
#endif