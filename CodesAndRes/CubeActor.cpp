#include "CubeActor.h"
CubeActor::CubeActor():itsNumber(0),itsPosition(point4(0,0,0,1))
{
}
CubeActor::CubeActor(int _input_number_):itsNumber(_input_number_),itsPosition(point4(0,0,0,1))
{
}
CubeActor::CubeActor(int _input_number_,point4 _input_position_):itsNumber(_input_number_),itsPosition(_input_position_)
{
}
CubeActor::~CubeActor()
{
}
void CubeActor::ChangePosition(point4 _input_position_)
{
	itsPosition = _input_position_;
}
void CubeActor::ChangePosition(float x,float y,float z)
{
	itsPosition = point4(x,y,z,1);
}