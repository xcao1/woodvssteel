#ifndef _CUBE_ACTOR_H_
#define _CUBE_ACTOR_H_
//
#include "Base.h"

#include "CubicObject.h"
//
class CubeActor
{
protected:
	//ATTRIBUTES:
	
	//used to track
	//Each Units has a number
	int itsNumber;
public:
	//hold the position value
	point4 itsPosition;
	//FUNCTIONS:
	//constructors
	CubeActor();
	CubeActor(int _input_number_);
	CubeActor(int _input_number_,point4 _input_position_);
	~CubeActor();
	//member functions
	//relate to controller
	void ChangePosition(point4 _input_position_);
	void ChangePosition(float x,float y,float z);
};
//
#endif