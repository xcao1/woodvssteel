#ifndef _CUBIC_OBJECT_H_
#define _CUBIC_OBJECT_H_
//
#include "Base.h"
//
//Used to draw cubes
class CubicObject
{
protected:
	//ATTRIBUTES:
	//8 points that defines a cube
	/*   
	       Y
           |
	       |
	       |
	       | 
           4________5
	      /|       /|
	     / |      / |
        6__|_____7  |
	    |  0_____|__1__________X
        | /      | /
	    |/       |/
		2________3
	   /
      /
     /
    Z

	point0------0,0,0
	point1------1,0,0
	point2------0,0,1
	point3------0,1,1
	point4------0,1,0
	point5------1,1,0
	point6------0,1,1
	point7------1,1,1
	*/
	point4 itspoints[8];
	//facet buffer
	//GL_QUADS
	vec4 itsfacetvertices[4*6];
	//wire buffer
	//GL_LINES
	vec4 itswirevertices[12*2];
	//size
	
	//RGBA
	color4 itsfacetcolor;
	color4 itswirecolor;
	//vao and vbo
	GLuint vaos[2];
	GLuint buffers[2];
	//programs
	GLuint facetprogram;
	GLuint wireprogram;

	GLubyte* image; 
	tex2    tex_coords[4*6];
	GLuint vTexCoord;
	GLsizei width;
	GLsizei height;
	GLuint texture;


public:
	float itssize;
	//FUNCTIONS:
	//constructors
	CubicObject();
	virtual ~CubicObject();
	//
	//member functions
	//functons that related to drawing
	virtual void OnInit();
	virtual void OnDisplay(mat4 _input_matrix_);
};
//
#endif