#include "Cursor.h"

Cursor::Cursor():attacking(false)
{
	movecolor = color4(0,1,0,1);
	attackcolor = color4(1,0,0,1);
	itswirecolor = color4(1,1,1,1);
	itssize = 0.4;
	//itspoints
	int index=0;
	
	itspoints[index]=point4(0,0,0,1);index++;//0
	itspoints[index]=point4(itssize,0,0,1);index++;//1
	itspoints[index]=point4(0,0,itssize,1);index++;//2
	itspoints[index]=point4(itssize,0,itssize,1);index++;//3
	itspoints[index]=point4(0,itssize,0,1);index++;//4
	itspoints[index]=point4(itssize,itssize,0,1);index++;//5
	itspoints[index]=point4(0,itssize,itssize,1);index++;//6
	itspoints[index]=point4(itssize,itssize,itssize,1);index++;//7

	//itsfacetvertices
	index=0;
	//facet 1 bottom
	itsfacetvertices[index]=itspoints[0];index++;//0
	itsfacetvertices[index]=itspoints[1];index++;//1
	itsfacetvertices[index]=itspoints[3];index++;//2
	itsfacetvertices[index]=itspoints[2];index++;//3
	//facet 2 top
	itsfacetvertices[index]=itspoints[4];index++;//4
	itsfacetvertices[index]=itspoints[5];index++;//5
	itsfacetvertices[index]=itspoints[7];index++;//5
	itsfacetvertices[index]=itspoints[6];index++;//7
	//facet 3
	itsfacetvertices[index]=itspoints[0];index++;//8
	itsfacetvertices[index]=itspoints[1];index++;//9
	itsfacetvertices[index]=itspoints[5];index++;//10
	itsfacetvertices[index]=itspoints[4];index++;//11
	//facet 4
	itsfacetvertices[index]=itspoints[0];index++;//12
	itsfacetvertices[index]=itspoints[4];index++;//13
	itsfacetvertices[index]=itspoints[6];index++;//14
	itsfacetvertices[index]=itspoints[2];index++;//15
	//facet 5
	itsfacetvertices[index]=itspoints[1];index++;//16
	itsfacetvertices[index]=itspoints[5];index++;//17
	itsfacetvertices[index]=itspoints[7];index++;//18
	itsfacetvertices[index]=itspoints[3];index++;//19
	//facet 6
	itsfacetvertices[index]=itspoints[2];index++;//20
	itsfacetvertices[index]=itspoints[6];index++;//21
	itsfacetvertices[index]=itspoints[7];index++;//22
	itsfacetvertices[index]=itspoints[3];index++;//23


	//itswirevertices
	index=0;
	//line 0
	itswirevertices[index]=itspoints[0];index++;
	itswirevertices[index]=itspoints[1];index++;
	//line 1
	itswirevertices[index]=itspoints[0];index++;
	itswirevertices[index]=itspoints[2];index++;
	//line 3
	itswirevertices[index]=itspoints[2];index++;
	itswirevertices[index]=itspoints[3];index++;
	//line 4
	itswirevertices[index]=itspoints[1];index++;
	itswirevertices[index]=itspoints[3];index++;


	//line 5
	itswirevertices[index]=itspoints[4];index++;
	itswirevertices[index]=itspoints[5];index++;
	//line 6
	itswirevertices[index]=itspoints[4];index++;
	itswirevertices[index]=itspoints[6];index++;
	//line 7
	itswirevertices[index]=itspoints[5];index++;
	itswirevertices[index]=itspoints[7];index++;
	//line 8
	itswirevertices[index]=itspoints[6];index++;
	itswirevertices[index]=itspoints[7];index++;


	//line 9
	itswirevertices[index]=itspoints[0];index++;
	itswirevertices[index]=itspoints[4];index++;
	//line 10
	itswirevertices[index]=itspoints[2];index++;
	itswirevertices[index]=itspoints[6];index++;
	//line 11
	itswirevertices[index]=itspoints[3];index++;
	itswirevertices[index]=itspoints[7];index++;
	//line 12
	itswirevertices[index]=itspoints[1];index++;
	itswirevertices[index]=itspoints[5];index++;
}

Cursor::~Cursor()
{
}

void Cursor::OnInit()
{
	//initialize buffers
	glGenBuffers( 2, buffers );
	
	//initialize VAOs
	glGenVertexArrays( 2, vaos );
	
	//initialize shaders
	facetprogram = InitShader( "cubefacet.v", "cubefacet.f" );
	wireprogram = InitShader( "cubeframe.v", "cubeframe.f" );

	GLuint vPosition;
	//vao0 cubefacet
	glUseProgram( facetprogram );
	glBindVertexArray( vaos[0] );
	glBindBuffer( GL_ARRAY_BUFFER, buffers[0] );
	glBufferData( GL_ARRAY_BUFFER, sizeof(itsfacetvertices), NULL, GL_STATIC_DRAW );
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(itsfacetvertices), itsfacetvertices );
	vPosition = glGetAttribLocation( facetprogram, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	
	//vao1 cubeframe
	glUseProgram( wireprogram );
	glBindVertexArray( vaos[1] );
	glBindBuffer( GL_ARRAY_BUFFER, buffers[1] );
	glBufferData( GL_ARRAY_BUFFER, sizeof(itswirevertices), NULL, GL_STATIC_DRAW );
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(itswirevertices), itswirevertices );
	vPosition = glGetAttribLocation( wireprogram, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	

}
void Cursor::OnDisplay(mat4 _input_matrix_)
{
	mat4 projection = mat4(1) * Perspective(90,1/1,1.0,40.0);
	
	glUseProgram( facetprogram );
	glUniformMatrix4fv(glGetUniformLocation( facetprogram, "ModelView" ), 1, GL_TRUE, _input_matrix_ );
	glUniformMatrix4fv(glGetUniformLocation( facetprogram, "Projection" ), 1, GL_TRUE, projection );
	switch(attacking)
	{
		case false://move
		{
			glUniform4fv(glGetUniformLocation(facetprogram, "color"), 1, movecolor);	
			break;
		}
		case true://attack
		{
			glUniform4fv(glGetUniformLocation(facetprogram, "color"), 1, attackcolor);
			break;
		}
	}
	glEnable(GL_POLYGON_OFFSET_FILL);
	glPolygonOffset(1.0, 1.0);
	glBindVertexArray( vaos[0] );
	glDrawArrays( GL_QUADS, 0, 4*6 );
	glDisable(GL_POLYGON_OFFSET_FILL);


	glUseProgram( wireprogram );
	glUniformMatrix4fv(glGetUniformLocation( wireprogram, "ModelView" ), 1, GL_TRUE, _input_matrix_ );
	glUniformMatrix4fv(glGetUniformLocation( wireprogram, "Projection" ), 1, GL_TRUE, projection );
	glUniform4fv(glGetUniformLocation(wireprogram, "color"), 1, itswirecolor);
	glBindVertexArray( vaos[1] );
	glDrawArrays( GL_LINES, 0, 12*2 );//draw the a line frame of the cube		
}
void Cursor::Attacking(bool _a)
{
	attacking = _a;
}