#ifndef _CURSOR_H_
#define _CURSOR_H_
//
#include "CubicObject.h"
//
class Cursor:public CubicObject
{
protected:
	//while attacking the cursor will turn red
	//while moving or selecting it will turn green
	//this store the status of the cursor
	//so that we can know if it is attacking or not
	bool attacking;
	//color that shows while moving
	color4 movecolor;
	//color that shows while attacking
	color4 attackcolor;
public:
	Cursor();
	~Cursor();
	virtual void OnInit();
	virtual void OnDisplay(mat4 _input_matrix_);
	virtual void Attacking(bool _a);
};
//
#endif