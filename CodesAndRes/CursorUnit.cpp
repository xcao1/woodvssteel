#include "CursorUnit.h"
CursorUnit::CursorUnit():angle(0)
{
	itsPosition = point4(0,0,1,1);
	itsStatus = false;

}
CursorUnit::~CursorUnit()
{
}
void CursorUnit::ChangeStatus(bool _input_status_)
{
	itsStatus = _input_status_;
	itsCursor.Attacking(itsStatus);
}
void CursorUnit::ChangePosition(point4 _input_position_)
{
	itsPosition = _input_position_;
}
void CursorUnit::ChangePosition(float x,float y,float z)
{
	itsPosition = point4(x,y,z,1);
}
void CursorUnit::Move(float x, float y)
{
	itsPosition.x +=x;
	itsPosition.y +=y;
}

void CursorUnit::OnInit()
{
	itsCursor.OnInit();
}
void CursorUnit::OnDisplay(mat4 _m)
{
	itsCursor.OnDisplay(_m);
}