#ifndef _CURSOR_UNIT_H_
#define _CURSOR_UNIT_H_
//
#include "Cursor.h"
//
class CursorUnit
{
protected:
public:
	point4 itsPosition;
	//Status whether attacking or not
	bool itsStatus;
	//Used to draw
	Cursor itsCursor;
	//every time the cursor rotate a few degree
	//the angle deals with the rotation
	float angle;
	//
	CursorUnit();
	~CursorUnit();
	void ChangeStatus(bool _input_status_);
	void ChangePosition(point4 _input_position_);
	void ChangePosition(float x,float y,float z);
	void Move(float x,float y);

	void OnInit();
	void OnDisplay(mat4 _m);
};
//
#endif