#include "GroundMesh.h"

GroundMesh::GroundMesh()
{
	color = color4(1,1,1,1);
	//16 *16
	for(int index = 0;index != 16;index++)
	{
		points[index] = point4(index,0,0,1);
	}
	for(int index = 0;index != 16;index++)
	{
		points[16+index] = point4(0,index,0,1);
	}
	for(int index = 0;index != 16;index++)
	{
		points[16+16+index] = point4(index,15,0,1);
	}
	for(int index = 0;index != 16;index++)
	{
		points[16+16+16+index] = point4(15,index,0,1);
	}
	int index = 0;int i = 0;
	while(index != 16)
	{
		meshvertices[i] = points[index];i++;
		meshvertices[i] = points[16+16+index];i++;
		index ++ ;
	}
	index = 0;
	while(index != 16)
	{
		meshvertices[i] = points[16+index];i++;
		meshvertices[i] = points[16+16+16+index];i++;
		index ++ ;
	}
}

GroundMesh::~GroundMesh()
{
}

void GroundMesh::OnInit()
{
	//initialize buffers
	glGenBuffers( 2, buffers );
	//initialize VAOs
	glGenVertexArrays( 2, vaos );
	//programs
	wireprogram = InitShader( "cubeframe.v", "cubeframe.f" );
	GLuint vPosition;
	//vao1 cubeframe
	glUseProgram( wireprogram );
	glBindVertexArray( vaos[0] );
	glBindBuffer( GL_ARRAY_BUFFER, buffers[0] );
	glBufferData( GL_ARRAY_BUFFER, sizeof(meshvertices), NULL, GL_STATIC_DRAW );
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(meshvertices), meshvertices );
	vPosition = glGetAttribLocation( wireprogram, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
}
void GroundMesh::OnDisplay(mat4 _input_matrix_)
{
	mat4 projection = mat4(1) * Perspective(90,1/1,1.0,40.0);

	glUseProgram( wireprogram );
	glUniformMatrix4fv(glGetUniformLocation( wireprogram, "ModelView" ), 1, GL_TRUE, _input_matrix_ );
	glUniformMatrix4fv(glGetUniformLocation( wireprogram, "Projection" ), 1, GL_TRUE, projection );
	glUniform4fv(glGetUniformLocation(wireprogram, "color"), 1, color);
	glBindVertexArray( vaos[0] );
	glDrawArrays( GL_LINES, 0, 16*2*2 );

}