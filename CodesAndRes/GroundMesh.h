#ifndef _GROUND_MESH_H_
#define _GROUND_MESH_H_
//
#include "Base.h"
//
//Used to draw ground meshes 
class GroundMesh
{
protected:
	point4 points[16*2*2];
	vec4 meshvertices[16*2*2];
	color4 color;
	//vao and vbo
	GLuint vaos[2];
	GLuint buffers[2];
	//programs
	GLuint wireprogram;
public:
	GroundMesh();
	~GroundMesh();
	virtual void OnInit();
	virtual void OnDisplay(mat4 _input_matrix_);
};
//
#endif