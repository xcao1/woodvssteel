#include "GroundMeshUnit.h"

GroundMeshUnit::GroundMeshUnit():itsMesh()
{
}
GroundMeshUnit::GroundMeshUnit(int _input_number_):itsMesh()
{
	itsNumber = _input_number_;
}
GroundMeshUnit::GroundMeshUnit(int _input_number_, point4 _input_position_):itsMesh()
{
	itsNumber = _input_number_;
	itsPosition = _input_position_;
}
GroundMeshUnit::~GroundMeshUnit()
{
}
void GroundMeshUnit::OnInit()
{
	itsMesh.OnInit();
}
void GroundMeshUnit::OnDisplay(mat4 _m)
{
	itsMesh.OnDisplay(_m);
}