#ifndef _GROUND_MESH_UNIT_H_
#define _GROUND_MESH_UNIT_H_
//
#include "Base.h"
#include "GroundMesh.h"
//
class GroundMeshUnit
{
protected:
public:
	int itsNumber;
	point4 itsPosition;
	GroundMesh itsMesh;
	GroundMeshUnit();
	GroundMeshUnit(int _input_number_);
	GroundMeshUnit(int _input_number_,point4 _input_position_);
	~GroundMeshUnit();

	void OnInit();
	void OnDisplay(mat4 _m);

};
//
#endif