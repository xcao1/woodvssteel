
#include "Angel.h"

Shader::Shader():filename(NULL), type(0), source(NULL)
{
}
Shader::Shader(const char *_filename,GLenum _type,GLchar *_source)
{
	filename = _filename;
	type = _type;
	source = _source;
}
Shader::~Shader()
{
}

namespace Angel {

// Create a NULL-terminated string by reading the provided file
//static
char* readShaderSource(const char* shaderFile)
{
	FILE* fp = fopen(shaderFile, "r");
    char* buf;
    long size;
    if(fp == NULL) return(NULL);
    fseek(fp, 0L, SEEK_END); 
	size = ftell(fp);
    fseek(fp, 0L, SEEK_SET); 
	buf = (char*) malloc((size + 1) * sizeof(char));
    fread(buf, 1, size, fp);
	buf[size] = '\0';  /* null termination */
    fclose(fp);

    return buf;
}


// Create a GLSL program object from vertex and fragment shader files
GLuint
InitShader(const char* vShaderFile, const char* fShaderFile)
{
	Shader vshader(vShaderFile, GL_VERTEX_SHADER, NULL);
	Shader fshader(fShaderFile, GL_FRAGMENT_SHADER, NULL);

	Shader *s[2];
	s[0] = &vshader;
	s[1] = &fshader;

    GLuint program = glCreateProgram();
    
    for ( int i = 0; i < 2; ++i ) 
	{
		s[i]->source = readShaderSource( s[i]->filename );
		
		if ( s[i]->source == NULL ) 
		{
			std::cerr << "Failed to read " << s[i]->filename << std::endl;
			exit( EXIT_FAILURE );
		}
	
		GLuint shader = glCreateShader( s[i]->type );
		glShaderSource( shader, 1, (const GLchar**) &s[i]->source, NULL );
		glCompileShader( shader );

		GLint  compiled;
		glGetShaderiv( shader, GL_COMPILE_STATUS, &compiled );
		if ( !compiled )
		{
			std::cerr << s[i]->filename << " failed to compile:" << std::endl;
			GLint  logSize;
			glGetShaderiv( shader, GL_INFO_LOG_LENGTH, &logSize );
			char* logMsg = new char[logSize];
			glGetShaderInfoLog( shader, logSize, NULL, logMsg );
			std::cerr << logMsg << std::endl;
			delete [] logMsg;
			int q; std::cin >> q;
			exit( EXIT_FAILURE );
		}
	
		delete [] s[i]->source;

		glAttachShader( program, shader );
    }

    /* link  and error check */
    glLinkProgram(program);

    GLint  linked;
    glGetProgramiv( program, GL_LINK_STATUS, &linked );
    if ( !linked ) {
	std::cerr << "Shader program failed to link" << std::endl;
	GLint  logSize;
	glGetProgramiv( program, GL_INFO_LOG_LENGTH, &logSize);
	char* logMsg = new char[logSize];
	glGetProgramInfoLog( program, logSize, NULL, logMsg );
	std::cerr << logMsg << std::endl;
	delete [] logMsg;

	exit( EXIT_FAILURE );
    }

    /* use program object */
    glUseProgram(program);

    return program;
}

}  // Close namespace Angel block
