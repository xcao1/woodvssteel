#include "Light.h"

Light::Light():
distant_light_position(point4(100.0, 100.0, 100.0, 0.0)),distant_light_ambient(color4(0.8, 0.8, 0.8, 1.0)),distant_light_diffuse(color4(1.0, 1.0, 1.0, 1.0)),distant_light_specular(color4(1.0, 1.0, 1.0, 1.0)),
point_light_position(point4(0.0, 0.0, -20.0, 1.0)),point_light_ambient(color4(1.0, 1.0, 1.0, 1.0)),point_light_diffuse(color4(1.0, 1.0, 1.0, 1.0)),point_light_specular(color4(1.0, 1.0, 1.0, 1.0))
{
}
Light::~Light()
{
}