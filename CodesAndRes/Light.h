#ifndef _LIGHT_H_
#define _LIGHT_H_
//
#include "Base.h"
//
class Light
{
protected:
public:
	//Light0 distant light outside
	point4 distant_light_position;
	color4 distant_light_ambient;
	color4 distant_light_diffuse;
	color4 distant_light_specular;

	//Light0 point light inside
	point4 point_light_position;
	color4 point_light_ambient;
	color4 point_light_diffuse;
	color4 point_light_specular;




	Light();
	~Light();
};
//
#endif