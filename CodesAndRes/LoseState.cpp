#include "LoseState.h"

LoseState::LoseState()
{
	GOTONEXT = 0;
	itsName = "Lose";
}
LoseState::~LoseState()
{
}
void LoseState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
}
void LoseState::Clear()
{
}
void LoseState::OnDisplay(SceneManager *_input_SceneManager_)
{
	
	//YOU LOSE
	_input_SceneManager_->itsMenuLose.itsMenu.OnDisplay(Scale(0.2,0.2,1) * Translate(-2,-2,-1.5) * Translate(_input_SceneManager_->itsMenuLose.itsPosition));
}
void LoseState::OnIdle(SceneManager *_input_SceneManager_)
{
	
}
void LoseState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch(key)
	{
	case 'z':
		{
			nextStateName = "Start";
			GOTONEXT = 1;
			MessageBox( NULL, _input_SceneManager_->itsScore.scorestr,"your score:", MB_OK);
			glutPostRedisplay();
			break;
		}
	}
}
