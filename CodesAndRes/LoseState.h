#ifndef _LOSE_STATE_H_
#define _LOSE_STATE_H_
//
#include "State.h"
//
//Tell the player he has lose
//and show the score
class LoseState:public State
{
protected:
public:
	LoseState();
	~LoseState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);

};
//
#endif