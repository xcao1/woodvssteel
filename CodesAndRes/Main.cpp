#include "Application.h"

//The application
Application itsApp;

void init(void)
{
	itsApp.init();
}
void display( void )
{
	itsApp.display();
}
void keyboard( unsigned char key, int x, int y )
{
	itsApp.keyboard(key);	
}
void specialkey(int key, int x, int y)
{ 
	itsApp.specialkey(key);
}
void idle(void)
{
	itsApp.idle();
}
void reshape( int width, int height )
{
	glViewport( 0, 0, (GLsizei) width, (GLsizei) height );
}
//Main Function
int main( int argc, char **argv )
{
	glutInit( &argc, argv );
	glutInitDisplayMode( GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH );
	glutInitWindowSize( 800, 800 );
	glutCreateWindow( "CS587 GAME ENGINE DESIGN" );
	glewInit();
	init();
	glutDisplayFunc( display );
	glutReshapeFunc( reshape );
	glutKeyboardFunc( keyboard );
	glutSpecialFunc( specialkey );
	glutIdleFunc(idle);
	glutMainLoop();
	return 0;
}