#include "Menu.h"

Menu::Menu()
{
	itspoints[0] = point4(0,0,0,1);texturecoordinates[0] = tex2(0,1);
	itspoints[1] = point4(5,0,0,1);texturecoordinates[1] = tex2(1,1);
	itspoints[2] = point4(5,5,0,1);texturecoordinates[2] = tex2(1,0);
	itspoints[3] = point4(0,5,0,1);texturecoordinates[3] = tex2(0,0);

}
Menu::~Menu()
{
}
void Menu::OnInit(int i)
{
	switch(i)
	{
	case 0:
		{
			//start
			image = glmReadPPM("menu0.ppm",width, height);
			break;
		}
	case 1:
		{
			//behaviour
			image = glmReadPPM("menu1.ppm",width, height);
			break;
		}
	case 2:
		{
			//wait or attack
			image = glmReadPPM("menu2.ppm",width, height);
			break;
		}
	case 3:
		{
			//win
			image = glmReadPPM("win.ppm",width,height);
			break;
		}
	case 4:
		{
			//lose
			image = glmReadPPM("lose.ppm",width,height);
			break;
		}
	}
	glGenTextures( 1, &texture );
	//initialize buffers
	glGenBuffers( 2, buffers );
	
	//initialize VAOs
	glGenVertexArrays( 2, vaos );

	//programs
	
	facetprogram = InitShader( "menu.v", "menu.f" );
	
	GLuint vPosition;
	//vao1 cubeframe
	glUseProgram( facetprogram );
	glBindVertexArray( vaos[0] );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texture );
	
	//glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    //glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGB,width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image );
	glBindBuffer( GL_ARRAY_BUFFER, buffers[0] );
	glBufferData( GL_ARRAY_BUFFER, sizeof(itspoints)+sizeof(texturecoordinates), NULL, GL_STATIC_DRAW );
	glBufferSubData( GL_ARRAY_BUFFER, 0, sizeof(itspoints), itspoints );
	glBufferSubData( GL_ARRAY_BUFFER, sizeof(itspoints),sizeof(texturecoordinates),texturecoordinates );
	vPosition = glGetAttribLocation( facetprogram, "vPosition" );
    glEnableVertexAttribArray( vPosition );
    glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0) );
	vTexCoord = glGetAttribLocation( facetprogram, "vTexCoord" );
    glEnableVertexAttribArray( vTexCoord );
    glVertexAttribPointer( vTexCoord, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(itspoints)) );
	// Set the value of the fragment shader texture sampler variable
    //   ("texture") to the the appropriate texture unit. In this case,
    //   zero, for GL_TEXTURE0 which was previously set by calling
    //   glActiveTexture().
    glUniform1i( glGetUniformLocation(facetprogram, "texture"), 0 );
}
void Menu::OnDisplay(mat4 _input_matrix_)
{
	mat4 projection = mat4(1) * Perspective(90,1/1,1.0,40.0);

	glUseProgram( facetprogram );
	glUniformMatrix4fv(glGetUniformLocation( facetprogram, "ModelView" ), 1, GL_TRUE, _input_matrix_ );
	glUniformMatrix4fv(glGetUniformLocation( facetprogram, "Projection" ), 1, GL_TRUE, projection );
	glBindVertexArray( vaos[0] );
	glActiveTexture( GL_TEXTURE0 );
	glBindTexture( GL_TEXTURE_2D, texture );
	glDrawArrays( GL_QUADS, 0, 4 );
}
