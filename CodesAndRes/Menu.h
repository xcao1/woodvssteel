#ifndef _MENU_H_
#define _MENU_H_
//
#include "Base.h"
//
//Used to draw menus
//such as unit behaviour selection
//or win,lose,start
class Menu
{
private:
protected:
	/*
	*/
	point4 itspoints[4];

	
	tex2 texturecoordinates[4];
	GLuint vTexCoord;
	GLsizei width;
	GLsizei height;
	GLuint texture;
	GLubyte *image;

	//vao and vbo
	GLuint vaos[2];
	GLuint buffers[2];
	//programs
	GLuint facetprogram;
public:
	//Menu();
	Menu();
	~Menu();
	virtual void OnInit(int i);
	virtual void OnDisplay(mat4 _input_matrix_);
};
//
#endif