#ifndef _MENU_UNIT_H_
#define _MENU_UNIT_H_
//
#include "Base.h"
#include "Menu.h"
//
class MenuUnit
{
protected:
	
public:
	point4 itsPosition;
	Menu itsMenu;

	MenuUnit();
	~MenuUnit();

	void OnInit(int n);
};
//
#endif