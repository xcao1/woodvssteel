#include "MoveState.h"

MoveState::MoveState()
{
	GOTONEXT = 0;
	itsName = "Move";
}
MoveState::~MoveState()
{
}
void MoveState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		//find out the active one
		if((*iterplayer).Active == 1)
		{
			itsiterplayer = iterplayer;
		}
	}
	//move cursor to unit
	_input_SceneManager_->itsCursorUnit.itsPosition.x = (*itsiterplayer).itsPosition.x;
	_input_SceneManager_->itsCursorUnit.itsPosition.y = (*itsiterplayer).itsPosition.y;
	
}
void MoveState::Clear()
{
}
void MoveState::OnDisplay(SceneManager *_input_SceneManager_)
{
}
void MoveState::OnIdle(SceneManager *_input_SceneManager_)
{

}
void MoveState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch (key)
	{
	case 'x':
		{
			//cancel order
			nextStateName = "PlayerUnitBehaviourSelect";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	case 'z':
		{
			//move to there
			(*itsiterplayer).itsPosition.x = _input_SceneManager_->itsCursorUnit.itsPosition.x;
			(*itsiterplayer).itsPosition.y = _input_SceneManager_->itsCursorUnit.itsPosition.y;
			(*itsiterplayer).moved = true;
			nextStateName = "WaitOrAttack";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	//control the camera
	case 'q':
		{
			_input_SceneManager_->itsCamera.control(0,0,5,0,0,false);
			glutPostRedisplay();
			break;
		}
	case 'Q':
		{
			_input_SceneManager_->itsCamera.control(0,0,-5,0,0,false);
			glutPostRedisplay();
			break;
		}
	case 'a':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,5,0,false);
			glutPostRedisplay();
			break;
		}
	case 'A':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,-5,0,false);
			glutPostRedisplay();
			break;
		}
	case 's':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,0,1,false);
			glutPostRedisplay();
			break;
		}
	case 'S':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,0,-1,false);
			glutPostRedisplay();
			break;
		}
	case ' ':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,0,0,true);
			glutPostRedisplay();
			break;
		}
	}
	
}
bool MoveState::Moveable(SceneManager *_input_SceneManager_,int x,int y)
{
	double TempDistance = 
		sqrt(pow(((_input_SceneManager_->itsCursorUnit.itsPosition.x + x) - (*itsiterplayer).itsPosition.x),2))+
		sqrt(pow(((_input_SceneManager_->itsCursorUnit.itsPosition.y + y) - (*itsiterplayer).itsPosition.y),2));
	//if too far away
	if(TempDistance > (*itsiterplayer).Speed)
	{
		return 0;
	}
	//if collide with other units
	else
	{
		for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
		{
			if((*iterplayer).Dead == false)
			{
				if((*iterplayer).itsPosition.x == (_input_SceneManager_->itsCursorUnit.itsPosition.x + x) &&
					(*iterplayer).itsPosition.y == (_input_SceneManager_->itsCursorUnit.itsPosition.y + y))
				{
					return 0;
				}
			}
		}
		for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
		iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
		{
			if((*iterAI).Dead == false)
			{
				if((*iterAI).itsPosition.x == (_input_SceneManager_->itsCursorUnit.itsPosition.x + x) &&
					(*iterAI).itsPosition.y == (_input_SceneManager_->itsCursorUnit.itsPosition.y + y))
				{
					return 0;
				}
			}
		}
		//able to move 
		return 1;
	}
	
}
void MoveState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
	switch( key )
	{
		//the object
		case GLUT_KEY_UP : 
			// MOVE FORWARD
			if(Moveable(_input_SceneManager_,0,1))
			{
				_input_SceneManager_->itsCursorUnit.Move(0,1);
				glutPostRedisplay();
			}
			break;
		case GLUT_KEY_DOWN :
			// MOVE WARD
			if(Moveable(_input_SceneManager_,0,-1))
			{
				_input_SceneManager_->itsCursorUnit.Move(0,-1);
				glutPostRedisplay();
			}
			break;
		case GLUT_KEY_LEFT:
			// MOVE LEFT
			if(Moveable(_input_SceneManager_,-1,0))
			{
				_input_SceneManager_->itsCursorUnit.Move(-1,0);
				glutPostRedisplay();
			}
			break;
		case GLUT_KEY_RIGHT:
			// MOVE RIGHT
			if(Moveable(_input_SceneManager_,1,0))
			{
				_input_SceneManager_->itsCursorUnit.Move(1,0);
				glutPostRedisplay();
			}
			break;
		
	}
}