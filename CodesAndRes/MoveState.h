#ifndef _MOVE_STATE_H_
#define _MOVE_STATE_H_
//
#include "Base.h"
#include "State.h"
#include "SceneManager.h"
//
//Deal with moving player's unit to an position
//the cursor pointed
//And check if the position is able to move to there
//if there is already some units
//or if the location is too far
//then it will be not able to move to there
class MoveState:public State
{
private:
protected:
	vector<PlayerUnit>::iterator itsiterplayer;

public:
	MoveState();
	~MoveState();
	bool Moveable(SceneManager *_input_SceneManager_,int x,int y);
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif