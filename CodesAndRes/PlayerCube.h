#ifndef _PLAYER_CUBE_H_
#define _PLAYER_CUBE_H_
//
#include "CubicObject.h"
//
class PlayerCube:public CubicObject
{
protected:
public:
	//danger affect the appearance of the cube
	//while danger equals true
	//the cube will turn red
	bool danger;
	PlayerCube();
	~PlayerCube();
	virtual void OnInit();
	virtual void OnDisplay(mat4 _input_matrix_);
	//function that set the value of danger
	void changestatus(bool _c);
};
//
#endif