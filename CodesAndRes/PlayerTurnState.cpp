#include "PlayerTurnState.h"

PlayerTurnState::PlayerTurnState()
{
	GOTONEXT = 0;
	itsName = "PlayerTurn";
}
PlayerTurnState::~PlayerTurnState()
{
}
void PlayerTurnState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
}
void PlayerTurnState::Clear()
{
}
void PlayerTurnState::OnDisplay(SceneManager *_input_SceneManager_)
{
}
void PlayerTurnState::OnIdle(SceneManager *_input_SceneManager_)
{
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Dead == false)
		{
			//recover
			(*iterplayer).HP += 1;
			//refresh
			(*iterplayer).Active = 0;
			(*iterplayer).moved = false;
		}
		
	}
	nextStateName = "PlayerUnitSelect";
	GOTONEXT = 1;
	glutPostRedisplay();
}
void PlayerTurnState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
}
void PlayerTurnState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}