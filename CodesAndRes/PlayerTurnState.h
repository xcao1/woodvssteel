#ifndef _PLAYER_TURN_STATE_H_
#define _PLAYER_TURN_STATE_H_
//
#include "State.h"
//
//Recovery every Player Units' HP
//Refresh Active to 0
class PlayerTurnState:public State
{
protected:
public:
	PlayerTurnState();
	~PlayerTurnState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif