#include "PlayerUnit.h"

PlayerUnit::PlayerUnit():CubeActor(),HP(10),ATK(2),Range(1),Speed(4),itsCube(),Active(0),Dead(false),moved(false)
{
}
PlayerUnit::PlayerUnit(int _input_number_):CubeActor(_input_number_),HP(10),ATK(2),Range(1),Speed(4),itsCube(),Active(0),Dead(false),moved(false)
{
}
PlayerUnit::PlayerUnit(int _input_number_,point4 _input_position_):CubeActor(_input_number_,_input_position_),HP(10),ATK(2),Range(1),Speed(4),Dead(false),itsCube(),Active(0),moved(false)
{
}
PlayerUnit::~PlayerUnit()
{
}
void PlayerUnit::ReSet()
{
	//reset
	HP = 10;
	ATK = 3;
	Range = 1;
	Speed = 4;
	Dead = false;
	Active = 0;
}

void PlayerUnit::OnInit()
{
	itsCube.OnInit();
}

void PlayerUnit::OnDisplay(mat4 _m)
{
	//if in danger ,warn
	if(HP < 5)
	{
		itsCube.changestatus(true);
	}
	else
	{
		itsCube.changestatus(false);
	}
	itsCube.OnDisplay(_m);
}