#ifndef _PLAYER_UNIT_H_
#define _PLAYER_UNIT_H_
//
#include "CubeActor.h"
#include "PlayerCube.h"
//
class PlayerUnit:public CubeActor
{
protected:
	
	
public:
	//HP store the health of the Unit
	//when HP below 0
	//the Unit will die
	int HP;
	//ATK store the damge a Unit can cause
	//it will be used to decrease enemy's HP
	//for example:
	//before attack, enemy HP = itsself
	//after attack, enemy HP = itself - your ATK
	int ATK;
	//The range that one can attack
	//if enemy is too far that out of the range
	//you can not attack it
	int Range;
	//The Speed affect the distance a Unit can move
	//In each Turn,a Unit can not move further than its Speed
	int Speed;
	//When the Unit dead
	//it will be set true
	bool Dead;
	//Related with Drawing
	PlayerCube itsCube;
	//Each Turn
	//At the beginning every Unit's Active will be set 0
	//When a Unit be selected or actived
	//which means it is now moveing or attacking
	//its Active will be set to 1
	//after it finished its turn
	//Active will be set to -1
	//Unitl next turn come
	//then it will be set to 0 again
	int Active;
	//This specially mark if a unit has moved or not
	//While a unit moved to a position and goes into 
	//the next state,the value will be set to true
	bool moved;
	PlayerUnit();
	PlayerUnit(int _input_number_);
	PlayerUnit(int _input_number_,point4 _input_position_);
	~PlayerUnit();
	
	//reset some value of the unit
	void ReSet();

	void OnInit();
	void OnDisplay(mat4 _m);
};
//
#endif