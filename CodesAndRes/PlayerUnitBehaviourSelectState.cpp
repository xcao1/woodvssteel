#include "PlayerUnitBehaviourSelectState.h"

PlayerUnitBehaviourSelectState::PlayerUnitBehaviourSelectState()
{
	GOTONEXT = 0;
	itsName = "PlayerUnitBehaviourSelect";
}
PlayerUnitBehaviourSelectState::~PlayerUnitBehaviourSelectState()
{
}
void PlayerUnitBehaviourSelectState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
}
void PlayerUnitBehaviourSelectState::Clear()
{
}
void PlayerUnitBehaviourSelectState::OnDisplay(SceneManager *_input_SceneManager_)
{
	//show menu
	_input_SceneManager_->itsMenuUnit1.itsMenu.OnDisplay(Scale(0.2,0.2,1) * Translate(-2,-2,-1.5) * Translate(_input_SceneManager_->itsMenuUnit1.itsPosition));
}
void PlayerUnitBehaviourSelectState::OnIdle(SceneManager *_input_SceneManager_)
{
}
void PlayerUnitBehaviourSelectState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch(key)
	{
	case 'x':
		{
			//cancel order,go back
			for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
			iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
			{
				if((*iterplayer).Active == 1)
				{
					(*iterplayer).Active = 0;
				}
			}
			nextStateName = "PlayerUnitSelect";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	case '1':
		{
			nextStateName = "Move";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	case '2':
		{
			nextStateName = "Attack";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	case '3':
		{
			nextStateName = "Wait";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}

	}
}
void PlayerUnitBehaviourSelectState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}