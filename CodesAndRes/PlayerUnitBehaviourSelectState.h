#ifndef _UNIT_BEHAVIOUR_H_
#define _UNIT_BEHAVIOUR_H_
//
#include "State.h"
//
//Select one of three behaviours
//move
//attack
//wait
//by press 
//1,2,3
class PlayerUnitBehaviourSelectState:public State
{
protected:
public:
	PlayerUnitBehaviourSelectState();
	~PlayerUnitBehaviourSelectState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif