#include "PlayerUnitSelectState.h"

PlayerUnitSelectState::PlayerUnitSelectState()
{
	GOTONEXT = 0;
	itsName = "PlayerUnitSelect";
}
PlayerUnitSelectState::~PlayerUnitSelectState()
{
}
void PlayerUnitSelectState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
	_input_SceneManager_->itsCursorUnit.ChangeStatus(false);
	_input_SceneManager_->itsCamera.moveto(_input_SceneManager_->itsCursorUnit.itsPosition);
	int i = 0;
	//check if all units has finished their action
	//if so, go to AI Turn
	int actived = 0;
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
			iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		i++;
		if((*iterplayer).Active == -1 || (*iterplayer).Dead == true)

		{
			actived++;
		}
	}
	if(actived >= i)
	{
		nextStateName = "AITurn";
		GOTONEXT = 1;
	}
}
void PlayerUnitSelectState::Clear()
{
}
void PlayerUnitSelectState::OnDisplay(SceneManager *_input_SceneManager_)
{
	
}
void PlayerUnitSelectState::OnIdle(SceneManager *_input_SceneManager_)
{
	
	glutPostRedisplay();
}
void PlayerUnitSelectState::OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key)
{
	switch( key )
	{
	case 'z':
		{
			//check same position
			for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
			iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
			{
				if((*iterplayer).itsPosition.x == _input_SceneManager_->itsCursorUnit.itsPosition.x
					&&(*iterplayer).itsPosition.y == _input_SceneManager_->itsCursorUnit.itsPosition.y)
				{
					if((*iterplayer).Dead == false)
					{
						if((*iterplayer).Active == 0)
						{
							//active it
							(*iterplayer).Active = 1;
						}
					}
				}
			}
			nextStateName = "PlayerUnitBehaviourSelect";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	//control the camera
	case 'q':
		{
			_input_SceneManager_->itsCamera.control(0,0,5,0,0,false);
			glutPostRedisplay();
			break;
		}
	case 'Q':
		{
			_input_SceneManager_->itsCamera.control(0,0,-5,0,0,false);
			glutPostRedisplay();
			break;
		}
	case 'a':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,5,0,false);
			glutPostRedisplay();
			break;
		}
	case 'A':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,-5,0,false);
			glutPostRedisplay();
			break;
		}
	case 's':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,0,1,false);
			glutPostRedisplay();
			break;
		}
	case 'S':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,0,-1,false);
			glutPostRedisplay();
			break;
		}
	case ' ':
		{
			_input_SceneManager_->itsCamera.control(0,0,0,0,0,true);
			glutPostRedisplay();
			break;
		}
	}
}
void PlayerUnitSelectState::OnSpecialKey(SceneManager *_input_SceneManager_,int key)
{
	
	switch( key )
	{
		case GLUT_KEY_UP :
			{
				// MOVE FORWARD
				_input_SceneManager_->itsCursorUnit.Move(0,1);
				_input_SceneManager_->itsCamera.control(0,1,0,0,0,false);
				glutPostRedisplay();
				break;
			}
		case GLUT_KEY_DOWN :
			{
				// MOVE WARD
				_input_SceneManager_->itsCursorUnit.Move(0,-1);
				_input_SceneManager_->itsCamera.control(0,-1,0,0,0,false);
				glutPostRedisplay();
				break;
			}
		case GLUT_KEY_LEFT:
			{
				// MOVE LEFT
				_input_SceneManager_->itsCursorUnit.Move(-1,0);
				_input_SceneManager_->itsCamera.control(-1,0,0,0,0,false);
				glutPostRedisplay();
				break;
			}
		case GLUT_KEY_RIGHT:
			{
				// MOVE RIGHT
				_input_SceneManager_->itsCursorUnit.Move(1,0);
				_input_SceneManager_->itsCamera.control(1,0,0,0,0,false);
				glutPostRedisplay();
				break;
			}
	}
}





