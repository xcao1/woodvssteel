#ifndef _PLAYER_UNIT_SELECET_STATE_H_
#define _PLAYER_UNIT_SELECET_STATE_H_
//
#include "State.h"
//
//Control the Cursor
//And use cursor to select the player's Unit
class PlayerUnitSelectState:public State
{
private:
protected:
public:
	PlayerUnitSelectState();
	~PlayerUnitSelectState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif