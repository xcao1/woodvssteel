#include "SceneManager.h"

SceneManager::SceneManager()
{
}
SceneManager::~SceneManager()
{
}

void SceneManager::AjustBackGround(point4 _input_position_)
{
	itsBackGround.itsPosition = _input_position_;
}

int SceneManager::CreateMeshUnit(point4 _input_position_)
{
	int i = 0;
	for(vector<GroundMeshUnit>::iterator iter =itsGroundMeshUnits.begin();
		iter != itsGroundMeshUnits.end();++iter)
	{
		i++;
	}
	//generate a unit and push it to the back of the vector
	GroundMeshUnit TempGroundMeshUnit(i,_input_position_);
	itsGroundMeshUnits.push_back(TempGroundMeshUnit);
	return i;
}
int SceneManager::CreatePlayerUnit(point4 _input_position_)
{
	int i = 0;
	for(vector<PlayerUnit>::iterator iter =itsPlayerUnits.begin();
		iter != itsPlayerUnits.end();++iter)
	{
		i++;
	}
	//generate a unit and push it to the back of the vector
	PlayerUnit TempPlayerUnit(i,_input_position_);
	itsPlayerUnits.push_back(TempPlayerUnit);
	return i;
}
int SceneManager::CreateAIUnit(point4 _input_position_)
{
	int i = 0;
	for(vector<AIUnit>::iterator iter =itsAIUnits.begin();
		iter != itsAIUnits.end();++iter)
	{
		i++;
	}
	//generate a unit and push it to the back of the vector
	AIUnit TempAIUnit(i,_input_position_);
	itsAIUnits.push_back(TempAIUnit);
	return i;
}

void SceneManager::OnInit()
{
	//initial all
	itsBackGround.OnInit();

	for(vector<GroundMeshUnit>::iterator itermesh = itsGroundMeshUnits.begin();
		itermesh != itsGroundMeshUnits.end();itermesh++)
	{
		(*itermesh).OnInit();
	}

	for(vector<PlayerUnit>::iterator iterplayer = itsPlayerUnits.begin();
		iterplayer != itsPlayerUnits.end();iterplayer++)
	{
		(*iterplayer).OnInit();
	}

	for(vector<AIUnit>::iterator iterAI = itsAIUnits.begin();
		iterAI != itsAIUnits.end();iterAI++)
	{
		(*iterAI).OnInit();
	}

	itsCursorUnit.OnInit();

	itsMenuUnit0.OnInit(0);
	itsMenuUnit1.OnInit(1);
	itsMenuUnit2.OnInit(2);
	itsMenuWin.OnInit(3);
	itsMenuLose.OnInit(4);
}

void SceneManager::DisplayItem()
{
	//diaplay the scene
	itsBackGround.OnDisplay(itsCamera.getmatrix() * Translate(itsBackGround.itsPosition));

	for(vector<GroundMeshUnit>::iterator itermesh = itsGroundMeshUnits.begin();
		itermesh != itsGroundMeshUnits.end();itermesh++)
	{
		(*itermesh).OnDisplay(itsCamera.getmatrix() * Translate((*itermesh).itsPosition) );
	}

	for(vector<PlayerUnit>::iterator iterplayer = itsPlayerUnits.begin();
		iterplayer != itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Dead == false)
		{
			(*iterplayer).OnDisplay(itsCamera.getmatrix() * Translate((*iterplayer).itsPosition) );
		}
	}
	for(vector<AIUnit>::iterator iterAI = itsAIUnits.begin();
			iterAI != itsAIUnits.end();iterAI++)
	{
		if((*iterAI).Dead == false)
		{
			(*iterAI).OnDisplay(itsCamera.getmatrix() * Translate((*iterAI).itsPosition) );
		}
	}

	//cursor
	itsCursorUnit.OnDisplay(
		itsCamera.getmatrix() *
		Translate(itsCursorUnit.itsPosition) *
		Translate(0.5,0.5,0) *
		RotateZ(itsCursorUnit.angle) * Translate(-itsCursorUnit.itsCursor.itssize/2,-itsCursorUnit.itsCursor.itssize/2,0));

}

void SceneManager::CursorRotate()
{
	//rotate the cursor
	itsCursorUnit.angle += 1;
}