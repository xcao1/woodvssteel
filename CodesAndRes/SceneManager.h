#ifndef _SCENEMANAGER_H_
#define _SCENEMANAGER_H_
//
#include "Base.h"

#include "PlayerUnit.h"
#include "AIUnit.h"
#include "CursorUnit.h"
#include "MenuUnit.h"
#include "GroundMeshUnit.h"
#include "BackGroundUnit.h"
#include "Sky.h"


#include "Camera.h"
#include "Scoring.h"
//
//Hold All the contents that
//can be interacted with human
//memu
//cursor
//camera
//score
//Player and AI Units
//.....
//And provide the functions to manipulate them
class SceneManager
{
protected:
	//ATTRIBUTES:
public:
	vector<PlayerUnit> itsPlayerUnits;
	vector<AIUnit> itsAIUnits;
	
	CursorUnit itsCursorUnit;
	MenuUnit itsMenuUnit0;
	MenuUnit itsMenuUnit1;
	MenuUnit itsMenuUnit2;
	MenuUnit itsMenuWin;
	MenuUnit itsMenuLose;

	
	vector<GroundMeshUnit> itsGroundMeshUnits;
	BackGroundUnit itsBackGround;
	Sky mSky;

	Camera itsCamera;

	Scoring itsScore;
	//FUNCTIONS:
	//constructors
	SceneManager();
	~SceneManager();
	//member functions
	int CreateMeshUnit(point4 _input_position_);
	int CreatePlayerUnit(point4 _input_position_);
	int CreateAIUnit(point4 _input_position_);

	void AjustBackGround(point4 _input_position_);

	void OnInit();
	void DisplayItem();
	void CursorRotate();
};

//
#endif