#include "SearchAndAttackState.h"

SearchAndAttackState::SearchAndAttackState()
{
	GOTONEXT = 0;
	itsName = "SearchAndAttack";
}
SearchAndAttackState::~SearchAndAttackState()
{
}
void SearchAndAttackState::Init(SceneManager *_input_SceneManager_)
{
	for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
			iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
	{
		//find out the active one
		if((*iterAI).Active == 1)
		{
			itsiterAI = iterAI;
		}
	}
	_input_SceneManager_->itsCamera.moveto((*itsiterAI).itsPosition);
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
			iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		//make sure dyingUnit is not a dead Unit
		if((*iterplayer).Dead == false)
		{
			dyingUnit = iterplayer;
			break;
		}
	}
	//find the player unit which has the lowes health
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
			iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Dead == false)
		{
			if((*dyingUnit).HP > (*iterplayer).HP)
			{
				dyingUnit = iterplayer;
			}
		}
	}
	GOTONEXT = 0;
}
void SearchAndAttackState::Clear()
{
}
void SearchAndAttackState::OnDisplay(SceneManager *_input_SceneManager_)
{
	
}
bool SearchAndAttackState::inRange(vector<PlayerUnit>::iterator iterplayer)
{
	//if in range
	double tempdistance = sqrt(pow(((*iterplayer).itsPosition.x - (*itsiterAI).itsPosition.x),2)) 
		+ sqrt(pow(((*iterplayer).itsPosition.y - (*itsiterAI).itsPosition.y),2));
	if(tempdistance > (*itsiterAI).Range)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}
bool SearchAndAttackState::Moveable(SceneManager *_input_SceneManager_,int x,int y)
{
	float move_x = x;
	float move_y = y;
	double speed = (*itsiterAI).Speed;
	double distance = sqrt(pow(move_x,2)) + sqrt(pow(move_y,2));
	//too far
	if(distance > speed)
	{
		return 0;
	}

	//avoid collision
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
	iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Dead == false)
		{
			if((*iterplayer).itsPosition.x == ((*itsiterAI).itsPosition.x + x) &&
				(*iterplayer).itsPosition.y == ((*itsiterAI).itsPosition.y + y))
			{
				return 0;
			}
		}
	}
	for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
	iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
	{
		if((*iterAI).Dead == false)
		{
			if((*iterAI).itsPosition.x == ((*itsiterAI).itsPosition.x + x) &&
				(*iterAI).itsPosition.y == ((*itsiterAI).itsPosition.y + y))
			{
				return 0;
			}
		}
	}
	//able to move 
	return 1;
}
void SearchAndAttackState::approaching(SceneManager *_input_SceneManager_)//0,0
{
	double speed = (*itsiterAI).Speed;
	//calculate the relative position between AI and player 's unit
	double difference_x = (*itsiterAI).itsPosition.x - (*dyingUnit).itsPosition.x;
	double difference_y = (*itsiterAI).itsPosition.y - (*dyingUnit).itsPosition.y;
	//distance
	double distance_x = sqrt(pow(difference_x,2));
	double distance_y = sqrt(pow(difference_y,2));
	double distance = distance_x + distance_y;

	//if further than speed,then,move as far as it can
	if(distance >= speed)
	{
		if( difference_x > 0)
		{
			if(difference_y > 0)
			{
				//x>0,y>0,AI is on the top right
				if(Moveable(_input_SceneManager_,-speed/2,-speed/2))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - speed/2;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - speed/2;
				}
			}
			else if(difference_y < 0)
			{
				//x>0,y<0,AI is on the bottom right
				if(Moveable(_input_SceneManager_,-speed/2,speed/2))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - speed/2;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + speed/2;
				}
			}
			else//difference_y = 0
			{
				if(Moveable(_input_SceneManager_,-speed,0))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - speed;
				}
			}
		}
		else if( difference_x < 0 )
		{
			if(difference_y > 0)
			{
				//x<0,y>0,AI is on the top left
				if(Moveable(_input_SceneManager_,speed/2,-speed/2))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + speed/2;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - speed/2;
				}
			}
			else if(difference_y < 0)
			{
				//x<0,y>0,AI is on the bottom left
				if(Moveable(_input_SceneManager_,speed/2,speed/2))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + speed/2;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + speed/2;
				}
			}
			else//difference_y = 0
			{
				if(Moveable(_input_SceneManager_,speed,0))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + speed;
				}
			}
		}
		else//difference_x = 0
		{
			if(difference_y > 0)
			{
				//AI is on the top
				if(Moveable(_input_SceneManager_,0,-speed))
				{
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - speed;
				}
			}
			else if(difference_y < 0)
			{
				//AI is below
				if(Moveable(_input_SceneManager_,0,speed))
				{
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + speed;
				}
			}
			else//difference_y = 0
			{
			}
		}
	}
	//if the distance is in the speed range,which means Ai is able to move to there
	//try to go nearby
	else if(distance > 1 && distance < speed)
	{
		if( difference_x > 0)
		{
			if(difference_y > 0)
			{
				if(Moveable(_input_SceneManager_,-distance_x,-(distance_y-1)))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - distance_x;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - (distance_y-1);
				}
				else if(Moveable(_input_SceneManager_,-(distance_x-1),-distance_y))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - (distance_x-1);
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - distance_y;
				}
			}
			else if(difference_y < 0)
			{
				if(Moveable(_input_SceneManager_,-distance_x,distance_y-1))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - distance_x;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + distance_y - 1;
				}
				else if(Moveable(_input_SceneManager_,-(distance_x-1),distance_y))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - (distance_x-1);
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + distance_y;
				}
			}
			else//difference_y = 0
			{
				if(Moveable(_input_SceneManager_,-(distance_x -1),0))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x - (distance_x-1);
				}
			}
		}
		else if( difference_x < 0 )
		{
			if(difference_y > 0)
			{
				if(Moveable(_input_SceneManager_,distance_x,-(distance_y-1)))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + distance_x;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - (distance_y-1);
				}
				else if(Moveable(_input_SceneManager_,distance_x-1,-distance_y))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + distance_x-1;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y - distance_y;
				}
			}
			else if(difference_y < 0)
			{
				if(Moveable(_input_SceneManager_,distance_x,distance_y-1))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + distance_x;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + distance_y-1;
				}
				else if(Moveable(_input_SceneManager_,distance_x-1,distance_y))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + distance_x-1;
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y + distance_y;
				}
			}
			else//difference_y = 0
			{
				if(Moveable(_input_SceneManager_,distance_x-1,0))
				{
					(*itsiterAI).itsPosition.x = (*itsiterAI).itsPosition.x + distance_x-1;
				}
			}
		}
		else//difference_x = 0
		{
			if(difference_y > 0)
			{
				if(Moveable(_input_SceneManager_,0,-(distance_y-1)))
				{
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y -(distance_y -1);
				}
			}
			else if(difference_y < 0)
			{
				if(Moveable(_input_SceneManager_,0,distance_y-1))
				{
					(*itsiterAI).itsPosition.y = (*itsiterAI).itsPosition.y +distance_y -1;
				}
			}
			else//difference_y = 0
			{
			}
		}
	}
}
void SearchAndAttackState::AttackIt(vector<PlayerUnit>::iterator iterplayer)
{
	//player lose health
	(*iterplayer).HP = (*iterplayer).HP - (*itsiterAI).ATK;
	if((*iterplayer).HP <= 0)
	{
		//dead
		(*iterplayer).Dead = true;
	}
}
void SearchAndAttackState::OnIdle(SceneManager *_input_SceneManager_)
{
	//if dying unit is in range
	//if so ,attack it
	if(inRange(dyingUnit))
	{
		//attack it
		AttackIt(dyingUnit);
		if((*dyingUnit).Dead == true)
		{
			_input_SceneManager_->itsScore.score -=5;
		}
	}
	else
	{
		//move toward it
		approaching(_input_SceneManager_);
		if(inRange(dyingUnit))
		{
			//attack it
			AttackIt(dyingUnit);
		}
		else
		{
			//attack other unit
			for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
				iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
			{
				if((*iterplayer).Dead == false)
				{
					if(inRange(iterplayer))
					{
						//attack it
						AttackIt(iterplayer);
					}
				}
			}
		}
	}
	//if all player's unit dead ,he will lose
	bool lose = true;
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Dead == false)
		{
			lose = false;
		}
	}
	if(lose)
	{
		//lose 20 points,because he lose
		_input_SceneManager_->itsScore.score -=20;
		_input_SceneManager_->itsScore.update();
		

		cout<<_input_SceneManager_->itsScore.scorestr<<endl;
		nextStateName = "Lose";
		GOTONEXT = 1;
		glutPostRedisplay();
	}
	else
	{
		
		(*itsiterAI).Active = -1;
		GOTONEXT = 1;
		nextStateName = "AIUnitQueue";
		glutPostRedisplay();
	}
}
void SearchAndAttackState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
}
void SearchAndAttackState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}