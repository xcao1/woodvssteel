#ifndef _SEARCH_AND_ATTACK_STATE_H_
#define _SEARCH_AND_ATTACK_STATE_H_
//
#include "State.h"
//
//This State Manipulate the behaviours
//of AI Unit,
//every time,each AI unit
//will find out a player's unit that has the lowest HP,
//this unit will be marked as a dying unit,
//all AI unit will try to attack it.
//The logic of the AI Unit is like this:
//Every time,check if it is able to attack the dying unit,
//which means if it is in the attacking range,
//if not,then try to get close to it,
//and check if it is in the attacking range again,
//if it is still out of range,
//then the AI will find the nearby Player's unit to attack,
//if it is in the attacking range
//After it finished its action,it will wait until next turn,
//which mean this states will go to "AIQueue"
class SearchAndAttackState:public State
{
protected:
public:
	vector<AIUnit>::iterator itsiterAI;
	vector<PlayerUnit>::iterator dyingUnit;
	SearchAndAttackState();
	~SearchAndAttackState();
	bool inRange(vector<PlayerUnit>::iterator iterplayer);
	bool Moveable(SceneManager *_input_SceneManager_,int x,int y);
	void approaching(SceneManager *_input_SceneManager_);
	void AttackIt(vector<PlayerUnit>::iterator iterplayer);
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif