#include "Sky.h"

Sky::Sky():pmodel(0),width(0),height(0),texture(0),image(0),facetprogram(0),lightPosition(point4(0,0,0,1))
{
}
Sky::~Sky()
{
}
void Sky::OnInit()
{
	//Loading the model
	pmodel = glmReadOBJ("sphere.obj");
	glmUnitize(pmodel);
	glmFacetNormals(pmodel);
	glmVertexNormals(pmodel, 90.0);
	glmSpheremapTexture(pmodel);
	glmLoadInVBO(pmodel);



	// Load shaders and use the resulting shader program
	program = InitShader( "pFshader_vert.glsl", "pFshader_frag.glsl" );
	glUseProgram( program );
	// set up vertex arrays
	glBindVertexArray( pmodel->vao );
	vPosition = glGetAttribLocation( program, "in_vertex" );
	glEnableVertexAttribArray( vPosition );
	glVertexAttribPointer( vPosition, 4, GL_FLOAT, GL_FALSE, 0,BUFFER_OFFSET(0) );
	vNormal = glGetAttribLocation( program, "in_normal" ); 
	glEnableVertexAttribArray( vNormal );
	glVertexAttribPointer( vNormal, 3, GL_FLOAT, GL_FALSE, 0,
			 BUFFER_OFFSET(sizeof(GLfloat)*4*pmodel->numPointsInVBO));

	glUniform4fv( glGetUniformLocation(program, "mat_ambient"),1, ambient_product );
	glUniform4fv( glGetUniformLocation(program, "mat_diffuse"),1, diffuse_product );
	glUniform4fv( glGetUniformLocation(program, "mat_specular"),1, specular_product );	
	glUniform4fv( glGetUniformLocation(program, "lightPosition"),1, lightPosition );
	glUniform1f( glGetUniformLocation(program, "mat_shininess"),material_shininess );
}
void Sky::OnDisplay(Angel::mat4 _input_matrix_)
{
}