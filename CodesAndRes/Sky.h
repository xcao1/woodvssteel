#ifndef _SKY_H_
#define _SKY_H_
//
#include "Base.h"
//
class Sky
{
protected:
	GLMmodel* pmodel;

	point4 lightPosition;

	GLsizei width;
	GLsizei height;
	GLuint texture;
	GLubyte *image;

	//programs
	GLuint program;
public:
	Sky();
	~Sky();

	virtual void OnInit();
	virtual void OnDisplay(mat4 _input_matrix_);
};
//
#endif