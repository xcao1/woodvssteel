#include "StartState.h"
PositionNode::PositionNode():x(0),y(0)
{
}
PositionNode::~PositionNode()
{
}
StartState::StartState()
{
	GOTONEXT = 0;
	itsName = "Start";
	PlayerPositionHeadNode = (PointerPositionNode)malloc(sizeof(PositionNode));;
	AIPositionHeadNode = (PointerPositionNode)malloc(sizeof(PositionNode));;

}
StartState::~StartState()
{
}
void StartState::FirstBegin(SceneManager *_input_SceneManager_)
{
	PositionNode *TempNodePointer;

	//store the position of every unit in the scene
	//the player units
	PlayerPositionHeadNode->nextNode = (PointerPositionNode)malloc(sizeof(PositionNode));
	TempNodePointer = PlayerPositionHeadNode->nextNode;
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		float x = (*iterplayer).itsPosition.x;
		float y = (*iterplayer).itsPosition.y;
		TempNodePointer->x = x;
		TempNodePointer->y = y;
		vector<PlayerUnit>::iterator tempiter1 = iterplayer + 1;
		if(tempiter1 != _input_SceneManager_->itsPlayerUnits.end())
		{
			TempNodePointer->nextNode = (PointerPositionNode)malloc(sizeof(PositionNode));
			TempNodePointer = TempNodePointer->nextNode;
		}
		else
		{
			TempNodePointer->nextNode = 0;
		}
	}
	//the ai units
	AIPositionHeadNode->nextNode = (PointerPositionNode)malloc(sizeof(PositionNode));
	TempNodePointer = AIPositionHeadNode->nextNode;
	for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
	iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
	{
		float x = (*iterAI).itsPosition.x;
		float y = (*iterAI).itsPosition.y;
		TempNodePointer->x = x;
		TempNodePointer->y = y;
		vector<AIUnit>::iterator tempiter2 = iterAI + 1;
		if(tempiter2 != _input_SceneManager_->itsAIUnits.end())
		{
			TempNodePointer->nextNode = (PointerPositionNode)malloc(sizeof(PositionNode));
			TempNodePointer = TempNodePointer->nextNode;
		}
		else
		{
			TempNodePointer->nextNode = 0;
		}
	}

}
void StartState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
}
void StartState::Clear()
{
}
void StartState::OnDisplay(SceneManager *_input_SceneManager_)
{
	//start menu
	_input_SceneManager_->itsMenuUnit0.itsMenu.OnDisplay(Scale(0.2,0.2,1) * Translate(-2,-2,-1.5) * Translate(_input_SceneManager_->itsMenuUnit0.itsPosition));
}
void StartState::OnIdle(SceneManager *_input_SceneManager_)
{
}
void StartState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch(key)
	{
	case 'z':
		{//start
			PositionNode *TempNodePointer;

			//Reset to Origin
			//reset player units
			TempNodePointer = PlayerPositionHeadNode->nextNode;
			for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
				iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
			{
				(*iterplayer).ReSet();
				(*iterplayer).itsPosition.x = TempNodePointer->x;
				(*iterplayer).itsPosition.y = TempNodePointer->y;
				TempNodePointer = TempNodePointer->nextNode;
			}
			//reset ai units
			TempNodePointer = AIPositionHeadNode->nextNode;
			for(vector<AIUnit>::iterator iterAI = _input_SceneManager_->itsAIUnits.begin();
			iterAI != _input_SceneManager_->itsAIUnits.end();iterAI++)
			{
				(*iterAI).ReSet();
				(*iterAI).itsPosition.x = TempNodePointer->x;
				(*iterAI).itsPosition.y = TempNodePointer->y;
				TempNodePointer = TempNodePointer->nextNode;
			}
			nextStateName = "PlayerTurn";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	}
}
void StartState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}