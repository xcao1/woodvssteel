#ifndef _START_STATE_H_
#define _START_STATE_H_
//
#include "State.h"
#include<stdlib.h>
//
//The Start State Reset every components into the beginning value
//And Stores the Scene,which means it will store the position of every units,
//so that after the game started again,
//The scene will be set to the beginning
//PositionNode will Store every units's position,and link them into a list
class PositionNode
{
protected:
public:
	float x;
	float y;
	class PositionNode *nextNode;
	PositionNode();
	~PositionNode();
};
typedef PositionNode * PointerPositionNode;
class StartState:public State
{
protected:
	PositionNode *PlayerPositionHeadNode;
	PositionNode *AIPositionHeadNode;
public:
	StartState();
	~StartState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
	void FirstBegin(SceneManager *_input_SceneManager_);
};
//
#endif 