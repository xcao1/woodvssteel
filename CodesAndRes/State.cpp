#include "State.h"

State::State()
{
	GOTONEXT = 0;
	itsName = "Original";
	nextStateName = "SelectState";
}
State::~State()
{
}
void State::Init(SceneManager *_input_SceneManager_)
{
}
void State::Clear()
{
}
void State::OnDisplay(SceneManager *_input_SceneManager_)
{
}
void State::OnIdle(SceneManager *_input_SceneManager_)
{
}
void State::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
}
void State::OnSpecialKey(SceneManager *_input_SceneManager_,int key)
{
}
