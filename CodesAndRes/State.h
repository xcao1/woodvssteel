#ifndef _STATE_H_
#define _STATE_H_
//
#include "Base.h"
#include "SceneManager.h"
#include <string>
//Base class of all states
class State
{
private:
protected:
public:
	bool GOTONEXT;
	std::string itsName;
	std::string nextStateName;
	//
	State();
	~State();
	//
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif