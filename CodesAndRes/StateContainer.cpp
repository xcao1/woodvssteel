#include "StateContainer.h"

StateContainer::StateContainer():mStart(),mPlayerTurn(),mPlayerUnitSelect(),mPlayerUnitBehaviourSelect(),
mMove(),mWaitOrAttack(),mAttack(),mWait(),mAITurn(),mAIUnitQueue(),mSearchAndAttack(),mWin(),mLose()
{
	pointerState = 0;
}
StateContainer::~StateContainer()
{
}
State *StateContainer::GetStateByName(std::string _input_state_name_)
{
	if(_input_state_name_ == "Start")
	{
		pointerState = &mStart;
	}
	else if(_input_state_name_ == "PlayerTurn")
	{
		pointerState = &mPlayerTurn;
		 
	}
	else if(_input_state_name_ == "PlayerUnitSelect")
	{
		pointerState = &mPlayerUnitSelect;
		 
	}
	else if(_input_state_name_ == "PlayerUnitBehaviourSelect")
	{
		pointerState = &mPlayerUnitBehaviourSelect;
		 
	}
	else if(_input_state_name_ == "Move")
	{
		pointerState = &mMove;
		 
	}
	else if(_input_state_name_ == "WaitOrAttack")
	{
		pointerState = &mWaitOrAttack;
		 
	}
	else if(_input_state_name_ == "Attack")
	{
		pointerState = &mAttack;
		 
	}
	else if(_input_state_name_ == "Wait")
	{
		pointerState = &mWait;
		 
	}
	else if(_input_state_name_ == "AITurn")
	{
		pointerState = &mAITurn;
		 
	}
	else if(_input_state_name_ == "AIUnitQueue")
	{
		pointerState = &mAIUnitQueue;
		 
	}
	else if(_input_state_name_ == "SearchAndAttack")
	{
		pointerState = &mSearchAndAttack;
		 
	}
	else if(_input_state_name_ ==  "Win")
	{
		pointerState = &mWin;
		 
	}
	else if(_input_state_name_ == "Lose")
	{
		pointerState = &mLose;
		 
	}
	return pointerState;
}