#ifndef _STATE_CONTAINER_H_
#define _STATE_CONTAINER_H_
//
#include "StartState.h"
#include "PlayerTurnState.h"
#include "PlayerUnitSelectState.h"
#include "PlayerUnitBehaviourSelectState.h"
#include "MoveState.h"
#include "WaitOrAttackState.h"
#include "AttackState.h"
#include "WaitState.h"
#include "AITurnState.h"
#include "AIUnitQueueState.h"
#include "SearchAndAttackState.h"
#include "WinState.h"
#include "LoseState.h"
/*
*/
//Store all possible states in the game
//provide the function that return the right state by a given name
//it will be holded as a member of the state machine
class StateContainer
{
private:
protected:
public:
	//ATTRIBUTES:
	StartState mStart;
	PlayerTurnState mPlayerTurn;
	PlayerUnitSelectState mPlayerUnitSelect;
	PlayerUnitBehaviourSelectState mPlayerUnitBehaviourSelect;
	
	MoveState mMove;
	WaitOrAttackState mWaitOrAttack;
	AttackState mAttack;
	WaitState mWait;

	
	AITurnState mAITurn;
	AIUnitQueueState mAIUnitQueue;
	SearchAndAttackState mSearchAndAttack;
	
	WinState mWin;
	LoseState mLose;

	State *pointerState;
	//FUNCTIONS:
	StateContainer();
	~StateContainer();
	State *GetStateByName(std::string _input_state_name_);
};
//
#endif