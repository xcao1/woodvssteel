#include "StateMachine.h"
StateMachine::StateMachine()
{
	CurrentState = 0;
}
StateMachine::~StateMachine()
{
}
void StateMachine::CheckChange(SceneManager *_input_SceneManager_)
{
	std::string tempStateName;
	if(CurrentState->GOTONEXT)
	{
		tempStateName = CurrentState->nextStateName;
		CurrentState->Clear();
		CurrentState = mStateContainer.GetStateByName(tempStateName);
		CurrentState->Init(_input_SceneManager_);
	}	  
}
void StateMachine::Begin(SceneManager *_input_SceneManager_)
{
	mStateContainer.mStart.FirstBegin(_input_SceneManager_);
	CurrentState = mStateContainer.GetStateByName("Start");
	CurrentState->Init(_input_SceneManager_);
}

void StateMachine::OnDisplay(SceneManager *_input_SceneManager_)
{
	CheckChange(_input_SceneManager_);
	CurrentState->OnDisplay(_input_SceneManager_);
}
void StateMachine::OnNormalKey(SceneManager *_input_SceneManager_,unsigned char key)
{
	CheckChange(_input_SceneManager_);
	CurrentState->OnKeyBoard(_input_SceneManager_,key);
}
void StateMachine::OnSpecialKey(SceneManager *_input_SceneManager_,int key)
{
	CheckChange(_input_SceneManager_);
	CurrentState->OnSpecialKey(_input_SceneManager_,key);
}
void StateMachine::OnIdle(SceneManager *_input_SceneManager_)
{
	CheckChange(_input_SceneManager_);
	CurrentState->OnIdle(_input_SceneManager_);
}