#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_
//
#include "Base.h"
#include "State.h"
#include "StateContainer.h"
//
//Manipulate the operation of the game,
//which hold all the states,the states are stored in the container,
//and the statemachine simply hold the containner,
//the state machine will checking if the state is going to change every time,
//if the state is going to change,then it will change the current state to the next state.
class StateMachine
{
private:
protected:
public:
	StateContainer mStateContainer;
	State *CurrentState;

	StateMachine();
	~StateMachine();
	void Begin(SceneManager *_input_SceneManager_);
	void CheckChange(SceneManager *_input_SceneManager_);

	void OnDisplay(SceneManager *_input_SceneManager_);
	void OnNormalKey(SceneManager *_input_SceneManager_,unsigned char key);
	void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
	void OnIdle(SceneManager *_input_SceneManager_);
};
//
#endif