#include "WaitOrAttackState.h"

WaitOrAttackState::WaitOrAttackState()
{
	GOTONEXT = 0;
	itsName = "WaitOrAttack";
}
WaitOrAttackState::~WaitOrAttackState()
{
}
void WaitOrAttackState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
}
void WaitOrAttackState::Clear()
{
}
void WaitOrAttackState::OnDisplay(SceneManager *_input_SceneManager_)
{
	//menu
	_input_SceneManager_->itsMenuUnit2.itsMenu.OnDisplay(Scale(0.2,0.2,1) * Translate(-2,-2,-1.5) * Translate(_input_SceneManager_->itsMenuUnit2.itsPosition));
}
void WaitOrAttackState::OnIdle(SceneManager *_input_SceneManager_)
{
}
void WaitOrAttackState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch(key)
	{
	case '2':
		{
			nextStateName = "Attack";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}
	case '3':
		{
			nextStateName = "Wait";
			GOTONEXT = 1;
			glutPostRedisplay();
			break;
		}

	}
}
void WaitOrAttackState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}