#ifndef _WAIT_OR_ATTACK_H_
#define _WAIT_OR_ATTACK_H_
//
#include "State.h"
//
//Choose wait or attack
class WaitOrAttackState:public State
{
protected:
public:
	WaitOrAttackState();
	~WaitOrAttackState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif