#include "WaitState.h"

WaitState::WaitState()
{
	GOTONEXT = 0;
	itsName = "Wait";
}
WaitState::~WaitState()
{
}
void WaitState::Init(SceneManager *_input_SceneManager_)
{
	//find out active one
	for(vector<PlayerUnit>::iterator iterplayer = _input_SceneManager_->itsPlayerUnits.begin();
		iterplayer != _input_SceneManager_->itsPlayerUnits.end();iterplayer++)
	{
		if((*iterplayer).Active == 1)
		{
			itsiterplayer = iterplayer;
		}
	}
	GOTONEXT = 0;
}
void WaitState::Clear()
{
}
void WaitState::OnDisplay(SceneManager *_input_SceneManager_)
{	
}
void WaitState::OnIdle(SceneManager *_input_SceneManager_)
{
	//set to -1,rest
	(*itsiterplayer).Active = -1;
	nextStateName = "PlayerUnitSelect";
	GOTONEXT = 1;
	glutPostRedisplay();
}
void WaitState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
}
void WaitState::OnSpecialKey(SceneManager *_input_SceneManager_, int key)
{
}