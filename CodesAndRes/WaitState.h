#ifndef _WAIT_STATE_H_
#define _WAIT_STATE_H_
//
#include "State.h"
//
//Set the Unit's Active to -1,
//And go to "PlayerUnitSelect"
class WaitState:public State
{
protected:
public:
	vector<PlayerUnit>::iterator itsiterplayer;

	WaitState();
	~WaitState();
	
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
	virtual void OnSpecialKey(SceneManager *_input_SceneManager_,int key);
};
//
#endif