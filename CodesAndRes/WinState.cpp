#include "WinState.h"

WinState::WinState()
{
	GOTONEXT = 0;
	itsName = "Win";
}
WinState::~WinState()
{
}
void WinState::Init(SceneManager *_input_SceneManager_)
{
	GOTONEXT = 0;
}
void WinState::Clear()
{
}
void WinState::OnDisplay(SceneManager *_input_SceneManager_)
{
	//YOU WIN
	_input_SceneManager_->itsMenuWin.itsMenu.OnDisplay(Scale(0.2,0.2,1) * Translate(-2,-2,-1.5) * Translate(_input_SceneManager_->itsMenuWin.itsPosition));
}
void WinState::OnIdle(SceneManager *_input_SceneManager_)
{
}
void WinState::OnKeyBoard(SceneManager *_input_SceneManager_, unsigned char key)
{
	switch(key)
	{
	case 'z':
		{
			nextStateName = "Start";
			GOTONEXT = 1;
			MessageBox( NULL, _input_SceneManager_->itsScore.scorestr,"your score:", MB_OK);
			glutPostRedisplay();
			break;
		}
	}
}
