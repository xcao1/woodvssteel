#ifndef _WIN_STATE_H_
#define _WIN_STATE_H_
//
#include "State.h"
//
//Tell the player he has won
//And show the score
class WinState:public State
{   
protected:
public:
	WinState();
	~WinState();
	virtual void Init(SceneManager *_input_SceneManager_);
	virtual void Clear();
	virtual void OnDisplay(SceneManager *_input_SceneManager_);
	virtual void OnIdle(SceneManager *_input_SceneManager_);
	virtual void OnKeyBoard(SceneManager *_input_SceneManager_,unsigned char key);
};
//
#endif