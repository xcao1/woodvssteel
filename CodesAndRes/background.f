in vec2 texcoord;
out vec4 fColor;
uniform sampler2D texture;
void main() 
{ 
    fColor = texture2D( texture, texcoord );
} 

