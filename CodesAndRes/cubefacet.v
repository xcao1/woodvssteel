in vec4 vPosition;
in vec2 vTexCoord;
out vec2 texcoord;
uniform mat4 ModelView;
uniform mat4 Projection;
void main()
{
	texcoord = vTexCoord;
    gl_Position = Projection*ModelView*vPosition;
}